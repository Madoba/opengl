#Disclaimer

This is my learning project for OpenGL, so expect to have bugs and/or not respect the good c++ pratices.
I am following the book OpenGL SuperBible 7th Edition.

#How to build
1) Clone the project:
    git clone --recursive https://gitlab.com/Madoba/opengl.git
2) Download premake.exe to opengl\vendor\bin
3) Execute scripts/Win-GenProjects.bat