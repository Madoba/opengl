#include "UniformBuffer.h"

namespace gl
{
	UniformBuffer::UniformBuffer(const size_t& size)
	{
		glGenBuffers(1, &m_Buffer);
		glBindBuffer(GL_UNIFORM_BUFFER, m_Buffer);
		glBufferData(GL_UNIFORM_BUFFER, size, NULL, GL_DYNAMIC_DRAW);
	}

	void UniformBuffer::Bind() const
	{
		glBindBufferBase(GL_UNIFORM_BUFFER, 0, m_Buffer);
	}

	void UniformBuffer::Unbind() const
	{
		glUnmapBuffer(GL_UNIFORM_BUFFER);
	}

	void* UniformBuffer::GetMapBuffer(const size_t& size)
	{
		glBindBufferBase(GL_UNIFORM_BUFFER, 0, m_Buffer);
		return glMapBufferRange(GL_UNIFORM_BUFFER, 0, size, GL_MAP_WRITE_BIT | GL_MAP_INVALIDATE_BUFFER_BIT);
	}

	void UniformBuffer::UnMap()
	{
		glUnmapBuffer(GL_UNIFORM_BUFFER);
	}

}