#include "IndexBuffer.h"

#include <glad/glad.h>


namespace gl
{
	IndexBuffer::IndexBuffer(const size_t& size)
	{
		glCreateBuffers(1, &m_VIB);
		glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, m_VIB);
		glBufferData(GL_ELEMENT_ARRAY_BUFFER, size, NULL, GL_STATIC_DRAW);
	}

	IndexBuffer::~IndexBuffer()
	{
		glDeleteBuffers(1, &m_VIB);
	}

	void IndexBuffer::Bind() const
	{
		glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, m_VIB);
	}

	void IndexBuffer::Unbind() const
	{
		glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, 0);
	}

	void IndexBuffer::SetData(const void* data, uint32_t size)
	{
		glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, m_VIB);
		glBufferSubData(GL_ELEMENT_ARRAY_BUFFER, m_Offset, size, data);
		m_Offset += size;
	}
}