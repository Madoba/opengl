#include "Grass.h"

#include <glm/gtc/matrix_transform.hpp>
#include <glm/gtc/type_ptr.hpp>

#include <iostream>

namespace gl
{
	Grass::Grass()
	{
		m_Shader = new Shader("assets/shaders/Grass.glsl");

		static const GLfloat grass_blade[] =
		{
			-0.3f, 0.0f,
			 0.3f, 0.0f,
			-0.20f, 1.0f,
			 0.1f, 1.3f,
			-0.05f, 2.3f,
			 0.0f, 3.3f
		};

		m_VAB = new VertexArrayBuffer(sizeof(grass_blade));
		m_VAB->SetData(grass_blade, sizeof(grass_blade));

		m_VAO = new VertexArray;
		m_VAB->SetLayout({
			{ BufferDataType::Float2, "vVertex" }
			});
		m_VAO->Bind();
		m_VAO->AddBuffer(m_VAB);

		glActiveTexture(GL_TEXTURE1);
		m_GrassLength = new Texture2D("assets/textures/grass_length.ktx");
		glActiveTexture(GL_TEXTURE2);
		m_GrassOrientation = new Texture2D("assets/textures/grass_orientation.ktx");
		glActiveTexture(GL_TEXTURE3);
		m_GrassColor = new Texture2D("assets/textures/grass_color.ktx");
		glActiveTexture(GL_TEXTURE4);
		m_GrassBend = new Texture2D("assets/textures/grass_bend.ktx");
	}

	Grass::~Grass()
	{
		delete m_Shader;
		delete m_VAO;
		delete m_VAB;
	}

	void Grass::Draw(double currentTime, float aspect)
	{
		float t = (float)currentTime * 0.02f;
		float r = 550.0f;

		static const GLfloat black[] = { 0.0f, 0.0f, 0.0f, 1.0f };
		static const GLfloat one = 1.0f;
		glClearBufferfv(GL_COLOR, 0, black);
		glClearBufferfv(GL_DEPTH, 0, &one);

		glm::mat4 mv_matrix = glm::lookAt(glm::vec3(sinf(t) * r, 25.0f, cosf(t) * r),
											glm::vec3(0.0f, -50.0f, 0.0f),
											glm::vec3(0.0f, 1.0f, 0.0f));
		glm::mat4 prj_matrix = glm::perspective(45.0f, aspect, 0.1f, 1000.0f);

		m_Shader->Bind();
		m_Shader->UploadUniformMat4(prj_matrix * mv_matrix, "mvpMatrix");

		glEnable(GL_DEPTH_TEST);
		glDepthFunc(GL_LEQUAL);

		m_VAO->Bind();
		glDrawArraysInstanced(GL_TRIANGLE_STRIP, 0, 6, 1024 * 1024);
		glDisable(GL_DEPTH_TEST);
	}
}