#pragma once
#include "Shader.h"
#include "VertexArray.h"
#include "IndexBuffer.h"
#include "UniformBuffer.h"

namespace gl
{
	class MultiViewport
	{
	public:
		MultiViewport();
		~MultiViewport();

		void Draw(double currentTime, uint32_t width, uint32_t height);

	private:
		Shader* m_Shader;
		VertexArray* m_VAO;
		VertexArrayBuffer* m_VAB;
		IndexBuffer* m_VIB;
		UniformBuffer* m_VUB;
	};
}