#pragma once

#include "VertexArray.h"
#include "VertexArrayBuffer.h"
#include "Shader.h"

namespace gl
{
	class SpinningCube
	{
	public:
		SpinningCube();
		~SpinningCube();

		void Draw(double currentTime, float aspect);
	private:
		void RendererInit();
		void RendererEnd();
	private:
		VertexArray* m_VAO;
		VertexArrayBuffer* m_VAB;
		Shader* m_Shader;
	};
}
