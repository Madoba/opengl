#pragma once
#include "Events/KeyCodes.h"
#include "VertexArray.h"
#include "VertexArrayBuffer.h"
#include "Texture2D.h"
#include "Shader.h"

namespace gl
{
	class MirrorClampEdge
	{
	public:
		enum class Mode
		{
			CLAMP_TO_BORDER = 1,
			MIRROR_CLAMP_TO_EDGE = 2
		};

	public:
		MirrorClampEdge(const Mode& displayMode = Mode::CLAMP_TO_BORDER);
		~MirrorClampEdge();

		void Draw(double currentTime);

	protected:
		
		
	private:
		Mode display_mode;
		VertexArray* m_VAO;
		VertexArrayBuffer* m_VAB;
		Shader* m_Shader;
		Texture2D* m_Texture2D;

		float m_ButtonPressedTime = 0.0f;
	};
}
