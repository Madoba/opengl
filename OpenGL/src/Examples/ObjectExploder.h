#pragma once
#include "Shader.h"
#include "Tools/sbmMeshLoader.h"

namespace gl
{
	class ObjectExploder
	{
	public:
		ObjectExploder();
		~ObjectExploder();

		void Draw(double currentTime, float aspect);

	private:
		Shader* m_Shader;
		sbmMesh m_Mesh;
	};
}
