#pragma once
#include "VertexArray.h"
#include "VertexArrayBuffer.h"
#include "Shader.h"

namespace gl
{
	class SpringMass
	{
	public:
		enum BUFFER_TYPE
		{
			POSITION_A,
			POSITION_B,
			VELOCITY_A,
			VELOCITY_B,
			CONNECTION
		};

	public:
		SpringMass();
		~SpringMass();

		void Draw();

		void OnImGuiRender();
	private:
		void LoadShader();
	private:
		static constexpr int points_x = 50;
		static constexpr int points_y = 50;
		static constexpr int points_total = points_x * points_y;
		static constexpr int connections_total = (points_x - 1) * points_y + (points_y - 1) * points_x;

		uint32_t m_VAO[2];
		uint32_t m_VBO[5];
		uint32_t m_VIB;
		uint32_t m_TexturePos[2];
		Shader* m_ShaderUpdate;
		Shader* m_ShaderRender;

		uint32_t m_IterationIndex = 0;
		int m_IterationsPerFrame = 16;

		bool m_DrawPoints = true;
		bool m_DrawLines = true;
	};

}
