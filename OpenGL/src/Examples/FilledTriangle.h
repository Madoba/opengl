#pragma once

#include "VertexArray.h"
#include "VertexArrayBuffer.h"
#include "Shader.h"

namespace gl {

	class FilledTriangle
	{
	public:
		FilledTriangle();
		~FilledTriangle();

		void Draw(double currentTime);

	private:
		VertexArray* m_VAO;
		VertexArrayBuffer* m_VAB;
		Shader* m_Shader;
	};

}
