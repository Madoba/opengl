#include "MultiDrawIndirect.h"

#include <glm/gtc/matrix_transform.hpp>

#include <imgui.h>

constexpr static int NUM_DRAWS = 50000;

namespace gl
{
	MultiDrawIndirect::MultiDrawIndirect()
	{
		LoadShaders();

		m_Object.load("assets/meshes/asteroids.sbm");
		size_t commandSize = NUM_DRAWS * sizeof(DrawArraysIndirectCommand);
		m_IDB = new IndirectDrawBuffer(commandSize);

		m_Command = (DrawArraysIndirectCommand*)m_IDB->GenerateMap(commandSize);
		for (int i = 0; i < NUM_DRAWS; i++)
		{
			m_Object.get_sub_object_info(i % m_Object.get_sub_object_count(),
				m_Command[i].first,
				m_Command[i].count);
				m_Command[i].primCount = 1;
				m_Command[i].baseInstance = i;
		}
		m_IDB->UnMap();
		
		m_VAO = new VertexArray(m_Object.get_vao());
		m_VAO->Bind();
		m_VAB = new VertexArrayBuffer(NUM_DRAWS * sizeof(uint32_t));
		uint32_t* draw_index = (uint32_t*)m_VAB->GenerateMap(NUM_DRAWS * sizeof(GLuint));
		for (int i = 0; i < NUM_DRAWS; i++)
		{
			draw_index[i] = i;
		}
		m_VAB->UnMap();

		glVertexAttribIPointer(10, 1, GL_UNSIGNED_INT, 0, NULL);
		glVertexAttribDivisor(10, 1);
		glEnableVertexAttribArray(10);
	}

	MultiDrawIndirect::~MultiDrawIndirect()
	{
		delete m_VAO;
		delete m_VAB;
		delete m_IDB;
		delete m_Shader;
	}

	void MultiDrawIndirect::Draw(double currentTime, float aspect)
	{
		glEnable(GL_DEPTH_TEST);
		glDepthFunc(GL_LEQUAL);
		glEnable(GL_CULL_FACE);

		static const float one = 1.0f;
		static const float black[] = { 0.0f, 0.0f, 0.0f, 0.0f };

		static double last_time = 0.0;
		static double total_time = 0.0;
		if (!m_Paused)
			total_time += (currentTime - last_time);
		last_time = currentTime;

		float t = float(total_time);
		int i = int(total_time * 3.0f);

		glClearBufferfv(GL_COLOR, 0, black);
		glClearBufferfv(GL_DEPTH, 0, &one);


		const glm::mat4 view_matrix = glm::lookAt(
			glm::vec3(100.0f * cosf(t * 0.023f), 100.0f * cosf(t * 0.023f), 300.0f * sinf(t * 0.037f) - 600.0f),
			glm::vec3(0.0f, 0.0f, 260.0f),
			glm::normalize(glm::vec3(0.1f - cosf(t * 0.1f) * 0.3f, 1.0f, 0.0f)));

		const glm::mat4 proj_matrix = glm::perspective(
			50.0f,
			aspect,
			1.0f,
			2000.0f);

		m_Shader->Bind();
		m_Shader->UploadUniformFloat(t, "time");
		m_Shader->UploadUniformMat4(view_matrix, "view_matrix");
		m_Shader->UploadUniformMat4(proj_matrix, "proj_matrix");
		m_Shader->UploadUniformMat4(proj_matrix * view_matrix, "viewproj_matrix");
		
		m_VAO->Bind();

		if (m_Mode == MODE::MODE_MULTIDRAW)
		{
			glMultiDrawArraysIndirect(GL_TRIANGLES, NULL, NUM_DRAWS, 0);
		}
		else if (m_Mode == MODE::MODE_SEPARATE_DRAWS)
		{
			for (int j = 0; j < NUM_DRAWS; j++)
			{
				GLuint first, count;
				m_Object.get_sub_object_info(j % m_Object.get_sub_object_count(), first, count);
				glDrawArraysInstancedBaseInstance(GL_TRIANGLES,
					first,
					count,
					1, j);
			}
		}

		glDisable(GL_DEPTH_TEST);
		glDisable(GL_CULL_FACE);
	}

	void MultiDrawIndirect::OnImGuiRender()
	{
		static char* pauseBtnLabel = "Pause";
		static char* vSyncBtnLabel = "VSync enable";
		static const char* items[] = { "MODE_MULTIDRAW", "MODE_SEPARATE_DRAWS" };
		static const char* current_item = items[1];
		ImGui::Begin("Properties");
		{
			if (ImGui::Button(pauseBtnLabel)) {
				m_Paused = !m_Paused;
				m_Paused ? pauseBtnLabel = "UnPause" : pauseBtnLabel = "Pause";
			}
			if (ImGui::Button(vSyncBtnLabel)) {
				m_VSync = !m_VSync;
				m_VSync ? vSyncBtnLabel = "VSync disable" : vSyncBtnLabel = "VSync enable";
			}

			if (ImGui::BeginCombo("##combo", current_item))
			{
				for (int n = 0; n < IM_ARRAYSIZE(items); n++)
				{
					bool is_selected = (current_item == items[n]);
					if (ImGui::Selectable(items[n], is_selected)) {
						current_item = items[n];
						if (n == 0)
							m_Mode = MODE::MODE_MULTIDRAW;
						else if(n == 1)
							m_Mode = MODE::MODE_SEPARATE_DRAWS;
					}
					if (is_selected) 
						ImGui::SetItemDefaultFocus();
				}
				ImGui::EndCombo();
			}


		}
		ImGui::End();
	}

	void MultiDrawIndirect::LoadShaders()
	{
		m_Shader = new Shader("assets/shaders/MultiDrawIndirect.glsl");
	}
}