#include "MirrorClampEdge.h"

#include <iostream>
#include "Events/KeyEvent.h"
#include "Events/Input.h"

namespace gl
{
	MirrorClampEdge::MirrorClampEdge(const Mode& displayMode)
		: display_mode(displayMode)
	{
		m_Texture2D = new Texture2D("assets/textures/rightarrows.ktx");
		m_Texture2D->Bind();
		m_Shader = new Shader("assets/shaders/MirrorClampEdge.glsl");

		static const float vertices[] = {	-1.0f, -1.0f, 0.0f, 1.0f,
											 1.0f, -1.0f, 0.0f, 1.0f,
											-1.0f,  1.0f, 0.0f, 1.0f,
											 1.0f,  1.0f, 0.0f, 1.0f };
		m_VAO = new VertexArray;
		m_VAO->Bind();
		m_VAB = new VertexArrayBuffer(sizeof(vertices));
		m_VAB->SetLayout({
			{ BufferDataType::Float4, "v_Position" }
		});
		m_VAB->SetData(vertices, sizeof(vertices));
		m_VAO->AddBuffer(m_VAB);
		m_VAO->Unbind();
	}

	MirrorClampEdge::~MirrorClampEdge()
	{
		delete m_VAO;
		delete m_VAB;
		delete m_Texture2D;
		delete m_Shader;
	}

	void MirrorClampEdge::Draw(double currentTime)
	{
		if (Input::IsKeyPressed(GL_KEY_M))
		{
			float timestep = currentTime - m_ButtonPressedTime;
			if (timestep > 0.5f) {
				m_ButtonPressedTime = currentTime;
				if (display_mode == Mode::CLAMP_TO_BORDER)
					display_mode = Mode::MIRROR_CLAMP_TO_EDGE;
				else
					display_mode = Mode::CLAMP_TO_BORDER;
			}
		}

		glActiveTexture(GL_TEXTURE0);
		m_Texture2D->Bind();
		m_VAO->Bind();
		m_Shader->Bind();

		switch (display_mode)
		{
			case Mode::CLAMP_TO_BORDER:
				glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_BORDER);
				glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_BORDER);
				break;
			case Mode::MIRROR_CLAMP_TO_EDGE:

				glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_MIRROR_CLAMP_TO_EDGE);
				glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_MIRROR_CLAMP_TO_EDGE);
				break;
		}
		glDrawArrays(GL_TRIANGLE_STRIP, 0, 4);
	}
}