#pragma once

#include "VertexArray.h"
#include "IndexBuffer.h"
#include "VertexArrayBuffer.h"
#include "Shader.h"

namespace gl {

	class IndexedCube
	{
	public:
		IndexedCube();
		~IndexedCube();

		void Draw(double currentTime, float aspect);

	private:
		VertexArray* m_VAO;
		VertexArrayBuffer* m_VAB;
		IndexBuffer* m_VIB;
		Shader* m_Shader;
	};

}
