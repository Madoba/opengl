#include "TextureKTX.h"

namespace gl
{
	TextureKTX::TextureKTX()
	{
		glActiveTexture(GL_TEXTURE0);
		m_Texture2D = new Texture2D("assets/textures/star.ktx");
		m_Shader = new Shader("assets/shaders/simpleTexture.glsl");

		static const float vertices[] = {	 1, -1, 0.5,
											-1, -1, 0.5,
											 1,  1, 0.5,

											 1,  1, 0.5,
											-1, -1, 0.5,
											-1,  1, 0.5
											};

		m_VAO = new VertexArray;
		m_VAO->Bind();
		m_VAB = new VertexArrayBuffer(sizeof(vertices));
		m_VAB->SetLayout({
			{ BufferDataType::Float3, "position" }
			});
		m_VAB->SetData(vertices, sizeof(vertices));
		m_VAO->AddBuffer(m_VAB);
		m_VAO->Unbind();
	}

	TextureKTX::~TextureKTX()
	{
		delete m_VAO;
		delete m_Shader;
		delete m_Texture2D;
	}

	void TextureKTX::Draw()
	{
		glActiveTexture(GL_TEXTURE0);
		m_Texture2D->Bind();
		m_Shader->Bind();
		m_VAO->Bind();
		glDrawArrays(GL_TRIANGLES, 0, 6);
		m_VAO->Unbind();
		m_Shader->Unbind();
		m_Texture2D->Unbind();

	}
}