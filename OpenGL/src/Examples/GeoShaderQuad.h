#pragma once
#include "Shader.h"
#include "VertexArray.h"

namespace gl
{
	class GeoShaderQuad
	{
		enum Mode {
			TRIANGLE_FAN = 0,
			LINES_ADJACENCY
		};
	public:
		GeoShaderQuad();
		~GeoShaderQuad();

		void Draw(double currentTime, float aspect);

		void OnImGuiRender();
	private:
		Shader* m_FanShader;
		Shader* m_LineAdjShader;
		VertexArray* m_VAO;

		bool m_Paused = false;
		Mode m_Mode = TRIANGLE_FAN;
	};
}
