#include "IndexedCube.h"

#include <glm/gtc/matrix_transform.hpp>

#define _USE_MATH_DEFINES
#include <math.h>

namespace gl
{
	IndexedCube::IndexedCube()
	{
		m_Shader = new Shader("assets/shaders/cube.glsl");
		static const GLfloat vertex_positions[] =
		{
			-0.25f, -0.25f, -0.25f,
			-0.25f,  0.25f, -0.25f,
			 0.25f, -0.25f, -0.25f,
			 0.25f,  0.25f, -0.25f,
			 0.25f, -0.25f,  0.25f,
			 0.25f,  0.25f,  0.25f,
			-0.25f, -0.25f,  0.25f,
			-0.25f,  0.25f,  0.25f
		};

		static const uint32_t vertex_indices[] =
		{
			0, 1, 2,
			2, 1, 3,
			2, 3, 4,
			4, 3, 5,
			4, 5, 6,
			6, 5, 7,
			6, 7, 0,
			0, 7, 1,
			6, 0, 2, 
			2, 4, 6,
			7, 5, 3,
			7, 3, 1
		};

		m_VAO = new VertexArray;
		m_VAO->Bind();
		m_VAB = new VertexArrayBuffer(sizeof(vertex_positions));
		m_VAB->SetLayout({
			{ BufferDataType::Float3, "position" }
			});
		m_VAB->SetData(vertex_positions, sizeof(vertex_positions));
		m_VAO->AddBuffer(m_VAB);

		m_VIB = new IndexBuffer(sizeof(vertex_indices));
		m_VIB->SetData(vertex_indices, sizeof(vertex_indices));
	}

	IndexedCube::~IndexedCube()
	{
		delete m_VAO;
		delete m_VAB;
		delete m_VIB;
		delete m_Shader;
	}

	void IndexedCube::Draw(double currentTime, float aspect)
	{
		glEnable(GL_CULL_FACE);
		//glFrontFace(GL_CW);

		glEnable(GL_DEPTH_TEST);
		glDepthFunc(GL_LEQUAL);

		m_VAO->Bind();
		m_VIB->Bind();
		glm::mat4 projection_matrix = glm::perspective(50.0f, aspect, 0.1f, 1000.0f);

		static const GLfloat one = 1.0f;
		glClearBufferfv(GL_DEPTH, 0, &one);

		m_Shader->Bind();
		m_Shader->UploadUniformMat4(projection_matrix, "proj_matrix");

#if 0
		float f = (float)currentTime * (float)M_PI * 0.1f;

		glm::mat4 mv_matrix = glm::translate(glm::mat4(1.0f), { 0.0f, 0.0f, -4.0f })
			* glm::translate(glm::mat4(1.0f), { sinf(2.1f * f) * 0.5f, cosf(1.7f * f) * 0.5f, sinf(1.3f * f) * cosf(1.5f * f) * 2.0f })
			* glm::rotate(glm::mat4(1.0f), glm::radians((float)currentTime * 45.0f), { 0.0f, 1.0f, 0.0f })
			* glm::rotate(glm::mat4(1.0f), glm::radians((float)currentTime * 81.0f), { 1.0f, 0.0f, 0.0f });

		// replicate with glm
		glm::mat4 view_matrix = (glm::mat4&)mv_matrix;

		m_Shader->UploadUniformMat4(view_matrix, "mv_matrix");
		m_Shader->UploadUniformMat4(projection_matrix, "proj_matrix");
		glDrawElements(GL_TRIANGLES, 36, GL_UNSIGNED_INT, 0);
#else
		for (int i = 0; i < 24; i++)
		{
			float f = (float)i + (float)currentTime * (float)M_PI * 0.1f;

			glm::mat4 mv_matrix = glm::translate(glm::mat4(1.0f), { 0.0f, 0.0f, -4.0f })
				* glm::translate(glm::mat4(1.0f), { sinf(2.1f * f) * 0.5f, cosf(1.7f * f) * 0.5f, sinf(1.3f * f) * cosf(1.5f * f) * 2.0f })
				* glm::rotate(glm::mat4(1.0f), glm::radians((float)currentTime * 45.0f), { 0.0f, 1.0f, 0.0f })
				* glm::rotate(glm::mat4(1.0f), glm::radians((float)currentTime * 81.0f), { 1.0f, 0.0f, 0.0f })
				* glm::scale(glm::mat4(1.0f), glm::vec3(0.1));


			glm::mat4 view_matrix = (glm::mat4&)mv_matrix;
			m_Shader->UploadUniformMat4(view_matrix, "mv_matrix");
			glDrawElements(GL_TRIANGLES, 36, GL_UNSIGNED_INT, 0);
		}
#endif
		glDisable(GL_CULL_FACE);
		glDisable(GL_DEPTH_TEST);
	}
}