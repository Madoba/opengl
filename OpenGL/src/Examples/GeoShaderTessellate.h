#pragma once
#include "Shader.h"
#include "VertexArray.h"
#include "IndexBuffer.h"

namespace gl
{
	class GeoShaderTessellate
	{
	public:
		GeoShaderTessellate();
		~GeoShaderTessellate();

		void Draw(double currentTime, float aspect);

	private:
		Shader* m_Shader;
		VertexArray* m_VAO;
		IndexBuffer* m_VIB;
	};
}
