#pragma once
#include "Shader.h"
#include "Tools/sbmMeshLoader.h"

namespace gl
{
	class NormalViewer
	{
	public:
		NormalViewer();
		~NormalViewer();

		void Draw(double currentTime, float aspect);

	private:
		Shader* m_Shader;
		sbmMesh m_Mesh;
	};
}
