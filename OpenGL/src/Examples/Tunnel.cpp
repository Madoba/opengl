#include "Tunnel.h"

#include <glm/gtc/matrix_transform.hpp>

namespace gl
{
	Tunnel::Tunnel()
	{
		m_Shader = new Shader("assets/shaders/Tunnel.glsl");
		m_TexWall = new Texture2D("assets/textures/brick.ktx");
		m_TexWall->SetFilterToMipmapLinear();
		m_TexFloor = new Texture2D("assets/textures/floor.ktx");
		m_TexFloor->SetFilterToMipmapLinear();
		m_TexCeiling = new Texture2D("assets/textures/ceiling.ktx");
		m_TexCeiling->SetFilterToMipmapLinear();

		static const float vertices[] = {	-0.5f, -0.5f, 0.0f, 1.0f,
											 0.5f, -0.5f, 0.0f, 1.0f,
											-0.5f,  0.5f, 0.0f, 1.0f,
											 0.5f,  0.5f, 0.0f, 1.0f };
		m_VAO = new VertexArray;
		m_VAO->Bind();
		m_VAB = new VertexArrayBuffer(sizeof(vertices));
		m_VAB->SetLayout({
			{ BufferDataType::Float4, "position" }
			});
		m_VAB->SetData(vertices, sizeof(vertices));
		m_VAO->AddBuffer(m_VAB);
		m_VAO->Unbind();
	}

	Tunnel::~Tunnel()
	{
		delete m_VAO;
		delete m_VAB;
		delete m_TexWall;
		delete m_TexFloor;
		delete m_TexCeiling;
		delete m_Shader;
	}

	void Tunnel::Draw(double currentTime, float aspect)
	{
		m_VAO->Bind();
		glm::mat4 projection_matrix = glm::perspective(50.0f, aspect, 0.1f, 100.0f);
		m_Shader->Bind();
		m_Shader->UploadUniformFloat((float)currentTime * 0.003f, "offset");

		std::vector<Texture2D*> textures = { m_TexWall , m_TexFloor ,m_TexWall , m_TexCeiling };
		for (int i = 0; i < textures.size(); i++) {
			glm::mat4 mv_matrix = glm::rotate(glm::mat4(1.0f), glm::radians(90.0f * (float)i), glm::vec3(0.0f, 0.0f, 1.0f))
				* glm::translate(glm::mat4(1.0f), glm::vec3(-0.5f, 0.0f, -10.0))
				* glm::rotate(glm::mat4(1.0f), glm::radians(90.0f), glm::vec3(0.0f, 1.0f, 0.0f))
				* glm::scale(glm::mat4(1.0f), glm::vec3(30.0f, 1.0f, 1.0f));
			glm::mat4 viewMatrix = projection_matrix * mv_matrix;
			m_Shader->UploadUniformMat4(viewMatrix, "viewMatrix");
			textures[i]->Bind();
			glDrawArrays(GL_TRIANGLE_STRIP, 0, 4);
		}

		m_Shader->Unbind();
		m_VAO->Unbind();
	}
}