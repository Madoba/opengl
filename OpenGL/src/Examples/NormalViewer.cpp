#include "NormalViewer.h"

#include <glm/gtc/matrix_transform.hpp>

namespace gl
{
	NormalViewer::NormalViewer()
	{
		m_Shader = new Shader("assets/shaders/NormalViewer.glsl");
		m_Mesh.load("assets/meshes/dragon.sbm");
	}

	NormalViewer::~NormalViewer()
	{
	}

	void NormalViewer::Draw(double currentTime, float aspect)
	{
		static const GLfloat black[] = { 0.0f, 0.0f, 0.0f, 1.0f };
		static const GLfloat one = 1.0f;

		glClearBufferfv(GL_COLOR, 0, black);
		glClearBufferfv(GL_DEPTH, 0, &one);

		glEnable(GL_DEPTH_TEST);
		glDepthFunc(GL_LEQUAL);

		glm::mat4 proj_matrix = glm::perspective(50.0f,
			aspect,
			0.1f,
			1000.0f);

		glm::mat4 mv_matrix = glm::translate(glm::mat4(1.0f), glm::vec3(0.0f, 0.0f, -60.0f)) *
			glm::rotate(glm::mat4(1.0f), glm::radians((float)currentTime * 15.0f), glm::vec3(0.0f, 1.0f, 0.0f)) *
			glm::rotate(glm::mat4(1.0f), glm::radians((float)currentTime * 21.0f), glm::vec3(1.0f, 0.0f, 0.0f));

		m_Shader->Bind();
		m_Shader->UploadUniformMat4(proj_matrix, "proj_matrix");
		m_Shader->UploadUniformMat4(mv_matrix, "mv_matrix");
		m_Shader->UploadUniformFloat(sinf((float)currentTime * 8.0f) * cosf((float)currentTime * 6.0f) * 0.03f + 0.05f, "normal_length");

		m_Mesh.render();
		glDisable(GL_DEPTH_TEST);
	}
}