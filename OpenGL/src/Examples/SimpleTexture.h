#pragma once

#include "VertexArray.h"
#include "VertexArrayBuffer.h"
#include "Texture2D.h"
#include "Shader.h"

namespace gl
{
	class SimpleTexture
	{
	public:
		SimpleTexture();
		~SimpleTexture();

		void Draw();

	protected:
		void generate_texture(float* data, uint32_t width, uint32_t height);

	private:
		VertexArray* m_VAO;
		VertexArrayBuffer* m_VAB;
		Shader* m_Shader;
		GLuint m_TextureID;
	};
}
