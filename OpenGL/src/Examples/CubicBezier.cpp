#include "CubicBezier.h"

#include <imgui.h>
#include <glm/gtc/matrix_transform.hpp>

namespace gl
{
	CubicBezier::CubicBezier()
	{
		m_Shader = new Shader("assets/shaders/CubicBezier.glsl");
		m_PointsShader = new Shader("assets/shaders/CubicBezier_Control_points.glsl");

		m_VAO = new VertexArray;
		m_VAO->Bind();

		m_VAB = new VertexArrayBuffer(sizeof(glm::vec3[16]));

		static const uint32_t indices[] =
		{
			0, 1, 1, 2, 2, 3,
			4, 5, 5, 6, 6, 7,
			8, 9, 9, 10, 10, 11,
			12, 13, 13, 14, 14, 15,

			0, 4, 4, 8, 8, 12,
			1, 5, 5, 9, 9, 13,
			2, 6, 6, 10, 10, 14,
			3, 7, 7, 11, 11, 15
		};

		m_VIB = new IndexBuffer(sizeof(indices));
		m_VIB->SetData(indices, sizeof(indices));
	}

	CubicBezier::~CubicBezier()
	{
		delete m_Shader;
		delete m_PointsShader;
		delete m_VAO;
		delete m_VAB;
		delete m_VIB;
	}

	void CubicBezier::Draw(double currentTime, float aspect)
	{
		static const GLfloat gray[] = { 0.1f, 0.1f, 0.1f, 0.0f };
		static const GLfloat one = 1.0f;

		int i;
		static double last_time = 0.0;
		static double total_time = 0.0;

		if (!m_Paused)
			total_time += (currentTime - last_time);
		last_time = currentTime;

		float t = (float)total_time;

		static const float patch_initializer[] =
		{
			-1.0f,  -1.0f,  0.0f,
			-0.33f, -1.0f,  0.0f,
			 0.33f, -1.0f,  0.0f,
			 1.0f,  -1.0f,  0.0f,

			-1.0f,  -0.33f, 0.0f,
			-0.33f, -0.33f, 0.0f,
			 0.33f, -0.33f, 0.0f,
			 1.0f,  -0.33f, 0.0f,

			-1.0f,   0.33f, 0.0f,
			-0.33f,  0.33f, 0.0f,
			 0.33f,  0.33f, 0.0f,
			 1.0f,   0.33f, 0.0f,

			-1.0f,   1.0f,  0.0f,
			-0.33f,  1.0f,  0.0f,
			 0.33f,  1.0f,  0.0f,
			 1.0f,   1.0f,  0.0f,
		};

		glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, 0, NULL);
		glEnableVertexAttribArray(0);

		glClearBufferfv(GL_COLOR, 0, gray);
		glClearBufferfv(GL_DEPTH, 0, &one);

		glEnable(GL_DEPTH_TEST);
		glm::vec3* p = (glm::vec3*)m_VAB->GenerateMap(sizeof(glm::vec3[16]));
		
		memcpy(p, patch_initializer, sizeof(patch_initializer));
		for (i = 0; i < 16; i++)
		{
			float fi = (float)i / 16.0f;
			p[i][2] = sinf(t * (0.2f + fi * 0.3f));
		}
		m_VAB->UnMap();
		m_VAO->Bind();
		m_VIB->Bind();
		m_Shader->Bind();

		glm::mat4 proj_matrix = glm::perspective(51.0f,
			aspect,
			1.0f, 1000.0f);
		glm::mat4 mv_matrix = glm::translate(glm::mat4(1.0f), glm::vec3(0.0f, 0.0f, -4.0f)) *
			glm::rotate(glm::mat4(1.0f), glm::radians(t * 10.0f), glm::vec3(0.0f, 1.0f, 0.0f)) *
			glm::rotate(glm::mat4(1.0f), glm::radians(t * 17.0f), glm::vec3(1.0f, 0.0f, 0.0f));

		m_Shader->UploadUniformMat4(mv_matrix, "mv_matrix");
		m_Shader->UploadUniformMat4(proj_matrix, "proj_matrix");
		m_Shader->UploadUniformMat4(proj_matrix * mv_matrix, "mvp");

		if (m_Wireframe)
			glPolygonMode(GL_FRONT_AND_BACK, GL_LINE);
		else
			glPolygonMode(GL_FRONT_AND_BACK, GL_FILL);

		glPatchParameteri(GL_PATCH_VERTICES, 16);
		glDrawArrays(GL_PATCHES, 0, 16);

		m_PointsShader->Bind();
		m_PointsShader->UploadUniformMat4(proj_matrix * mv_matrix, "mvp");
		if (m_ShowPoints)
		{
			glPointSize(9.0f);
			m_PointsShader->UploadUniformFloat4(glm::vec4(0.2f, 0.7f, 0.9f, 1.0f), "draw_color");
			glDrawArrays(GL_POINTS, 0, 16);
		}
		if (m_ShowCage)
		{
			m_PointsShader->UploadUniformFloat4(glm::vec4(0.7f, 0.9f, 0.2f, 1.0f), "draw_color");
			glDrawElements(GL_LINES, 48, GL_UNSIGNED_INT, NULL);
		}
		glPolygonMode(GL_FRONT_AND_BACK, GL_FILL);
		glDisable(GL_DEPTH_TEST);
	}

	void CubicBezier::OnImGuiRender()
	{
		static char* pauseBtnLabel = "Pause";
		ImGui::Begin("Cubic Bezier");
		{
			if (ImGui::Button(pauseBtnLabel)) {
				m_Paused = !m_Paused;
				m_Paused ? pauseBtnLabel = "UnPause" : pauseBtnLabel = "Pause";
			}
			ImGui::Checkbox("Wireframe", &m_Wireframe);
			ImGui::Checkbox("Show points", &m_ShowPoints);
			ImGui::Checkbox("Show cage", &m_ShowCage);

		}
		ImGui::End();
	}
}