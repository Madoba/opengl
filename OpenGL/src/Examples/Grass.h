#pragma once
#include "VertexArray.h"
#include "VertexArrayBuffer.h"
#include "Shader.h"
#include "Texture2D.h"

namespace gl
{
	class Grass
	{
	public:
		Grass();
		~Grass();

		void Draw(double currentTime, float aspect);
	private:
		VertexArray* m_VAO;
		VertexArrayBuffer* m_VAB;
		Shader* m_Shader;
		Texture2D* m_GrassLength;
		Texture2D* m_GrassOrientation;
		Texture2D* m_GrassColor;
		Texture2D* m_GrassBend;
	};
}
