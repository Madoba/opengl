#pragma once
#include "Shader.h"
#include "VertexArray.h"
#include "VertexArrayBuffer.h"
#include "IndexBuffer.h"

namespace gl
{
	class CubicBezier
	{
	public:
		CubicBezier();
		~CubicBezier();

		void Draw(double currentTime, float aspect);

		void OnImGuiRender();
	private:
		Shader* m_Shader;
		Shader* m_PointsShader;
		VertexArray* m_VAO;
		VertexArrayBuffer* m_VAB;
		IndexBuffer* m_VIB;

		bool m_Paused = false;
		bool m_Wireframe = false;
		bool m_ShowPoints = false;
		bool m_ShowCage = false;
	};
}
