#pragma once
#include "Shader.h"
#include "Tools/sbmMeshLoader.h"

namespace gl
{
	class ClipDistance
	{
	public:
		ClipDistance();
		~ClipDistance();

		void Draw(double currentTime, float aspect);

		void OnImGuiRender();
	private:
		Shader* m_Shader;
		sbmMesh m_Mesh;

		bool m_Paused = false;
	};
}
