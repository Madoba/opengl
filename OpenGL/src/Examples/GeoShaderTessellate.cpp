#include "GeoShaderTessellate.h"

#include <glm/gtc/matrix_transform.hpp>

namespace gl
{
	GeoShaderTessellate::GeoShaderTessellate()
	{
		m_Shader = new Shader("assets/shaders/GeoShaderTessellate.glsl");
        static const float tetrahedron_verts[] =
        {
             0.000f,  0.000f,  1.000f,
             0.943f,  0.000f, -0.333f,
            -0.471f,  0.816f, -0.333f,
            -0.471f, -0.816f, -0.333f
        };

        static const uint32_t tetrahedron_indices[] =
        {
            0, 1, 2,
            0, 2, 3,
            0, 3, 1,
            3, 2, 1
        };
        m_VAO = new VertexArray;
        m_VAO->Bind();
        m_VIB = new IndexBuffer(sizeof(tetrahedron_indices) + sizeof(tetrahedron_verts));
        m_VIB->SetData(tetrahedron_indices, sizeof(tetrahedron_indices));
        m_VIB->SetData(tetrahedron_verts, sizeof(tetrahedron_verts));
        glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, 0, (GLvoid*)(sizeof(uint32_t) * 12));
        glEnableVertexAttribArray(0);
	}

	GeoShaderTessellate::~GeoShaderTessellate()
	{
        delete m_Shader;
        delete m_VAO;
        delete m_VIB;
	}

	void GeoShaderTessellate::Draw(double currentTime, float aspect)
	{
        static const GLfloat black[] = { 0.0f, 0.0f, 0.0f, 1.0f };
        static const GLfloat one = 1.0f;

        glClearBufferfv(GL_COLOR, 0, black);
        glClearBufferfv(GL_DEPTH, 0, &one);

        glEnable(GL_CULL_FACE);
        glEnable(GL_DEPTH_TEST);
        glDepthFunc(GL_LEQUAL);

        glm::mat4 proj_matrix = glm::perspective(55.0f,
            aspect,
            0.1f,
            1000.0f);

        glm::mat4 mv_matrix = glm::translate(glm::mat4(1.0f), glm::vec3(0.0f, 0.0f, -8.0f)) *
            glm::rotate(glm::mat4(1.0f), 
                glm::radians((float)currentTime * 71.0f), 
                glm::vec3(0.0f, 1.0f, 0.0f)) *
            glm::rotate(glm::mat4(1.0f), glm::radians((float)currentTime * 10.0f), glm::vec3(1.0f, 0.0f, 0.0f));

        m_Shader->Bind();
        m_Shader->UploadUniformMat4(proj_matrix * mv_matrix, "mvpMatrix");
        m_Shader->UploadUniformMat4(mv_matrix, "mvMatrix");
        m_Shader->UploadUniformFloat(sinf(currentTime * 4.0f) * 0.75f + 1.0f, "stretch");
        m_VIB->Bind();
        m_VAO->Bind();

        glDrawElements(GL_TRIANGLES, 12, GL_UNSIGNED_INT, NULL);

        glDisable(GL_CULL_FACE);
        glDisable(GL_DEPTH_TEST);
	}
}