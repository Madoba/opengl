#pragma once
#include "VertexArray.h"
#include "UniformBuffer.h"
#include "Texture2DArray.h"
#include "Shader.h"

namespace gl
{
	class AlienRain
	{
	public:
		AlienRain();
		~AlienRain();

		void Draw(double currentTime);

	private:
		VertexArray* m_VAO;
		UniformBuffer* m_UB;
		Shader* m_Shader;
		Texture2DArray* m_Texture2DArray;

		float           droplet_x_offset[256];
		float           droplet_rot_speed[256];
		float           droplet_fall_speed[256];
	};

}
