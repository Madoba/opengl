#include "WrapModes.h"

namespace gl
{

	WrapModes::WrapModes()
	{
		m_Texture2D = new Texture2D("assets/textures/rightarrows.ktx");
		m_Texture2D->Bind();
		m_Shader = new Shader("assets/shaders/WrapMode.glsl");

		static const float vertices[] = {	-0.45f, -0.45f, 0.5f, 1.0f,
											 0.45f, -0.45f, 0.5f, 1.0f,
											-0.45f,  0.45f, 0.5f, 1.0f,
											 0.45f,  0.45f, 0.5f, 1.0f };
		m_VAO = new VertexArray;
		m_VAO->Bind();
		m_VAB = new VertexArrayBuffer(sizeof(vertices));
		m_VAB->SetLayout({
			{ BufferDataType::Float4, "v_Position" }
			});
		m_VAB->SetData(vertices, sizeof(vertices));
		m_VAO->AddBuffer(m_VAB);
		m_VAO->Unbind();
	}

	WrapModes::~WrapModes()
	{
		delete m_VAO;
		delete m_VAB;
		delete m_Texture2D;
		delete m_Shader;
	}

	void WrapModes::Draw()
	{
		static const GLfloat yellow[] = { 0.4f, 0.4f, 0.0f, 1.0f };
		static const GLenum wrapmodes[] = { GL_CLAMP_TO_EDGE, GL_REPEAT, GL_CLAMP_TO_BORDER, GL_MIRRORED_REPEAT };

		std::vector<glm::vec2> offsets = { glm::vec2(-0.5f, -0.5f),
										   glm::vec2(0.5f, -0.5f),
										  glm::vec2 (-0.5f,  0.5f),
										   glm::vec2(0.5f,  0.5f) };
		m_Texture2D->Bind();
		m_Shader->Bind();
		m_VAO->Bind();
		glTexParameterfv(GL_TEXTURE_2D, GL_TEXTURE_BORDER_COLOR, yellow);

		for (int i = 0; i < 4; i++)
		{
			m_Shader->UploadUniformFloat2(offsets[i], "offset");

			glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, wrapmodes[i]);
			glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, wrapmodes[i]);

			glDrawArrays(GL_TRIANGLE_STRIP, 0, 4);
		}
	}

}