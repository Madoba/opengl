#include "FilledTriangle.h"

#include <iostream>

namespace gl {



	FilledTriangle::FilledTriangle()
	{
		m_Shader = new Shader("assets/shaders/FlatColor.glsl");

		static const float vertices[] = { 
											 0.25, -0.25, 0.5,
											-0.25, -0.25, 0.5,
											 0.25,  0.25, 0.5 };

		m_VAO = new VertexArray;
		m_VAO->Bind();
		m_VAB = new VertexArrayBuffer(sizeof(vertices));
		m_VAB->SetLayout({
			{ BufferDataType::Float3, "u_Position" }
			});
		m_VAB->SetData(vertices, sizeof(vertices));
		m_VAO->AddBuffer(m_VAB);
		m_VAO->Unbind();
	}

	FilledTriangle::~FilledTriangle()
	{
		delete m_VAB;
		delete m_VAO;
		delete m_Shader;
	}

	void FilledTriangle::Draw(double currentTime)
	{
		glm::vec4 color = { (float)sin(currentTime) * 0.5f + 0.5f,
							(float)cos(currentTime) * 0.5f + 0.5f,
							0.0f, 1.0f };

		m_VAO->Bind();
		m_Shader->Bind();
		m_Shader->UploadUniformFloat4(color, 1);
		glDrawArrays(GL_TRIANGLES, 0, 3);
		m_Shader->Unbind();
		m_VAO->Unbind();
	}

}