#include "TessellatedTriangle.h"

namespace gl {

	TessellatedTriangle::TessellatedTriangle()
	{
		m_Shader = new Shader("assets/shaders/TessellationShader.glsl");

		static const float vertices[] = { 0.25, -0.25, 0.5,
											-0.25, -0.25, 0.5,
											 0.25,  0.25, 0.5 };

		m_VAO = new VertexArray;
		m_VAO->Bind();
		m_VAB = new VertexArrayBuffer(sizeof(vertices));
		m_VAB->SetData(vertices, sizeof(vertices));
		m_VAO->AddBuffer(m_VAB);
		m_VAO->Unbind();
	}

	TessellatedTriangle::~TessellatedTriangle()
	{
		delete m_VAO;
		delete m_VAB;
		delete m_Shader;
	}

	void TessellatedTriangle::Draw(double currentTime)
	{
		glm::vec4 offset = { (float)sin(currentTime) * 0.5f,
								(float)cos(currentTime) * 0.6f,
								0.0f, 0.0f };
		glm::vec4 color = { (float)sin(currentTime) * 0.5f + 0.5f,
							(float)cos(currentTime) * 0.5f + 0.5f,
							0.0f, 1.0f };


		m_VAO->Bind();
		
		m_Shader->Bind();
		m_Shader->UploadUniformFloat4(offset, "u_offset");
		m_Shader->UploadUniformFloat4(color, "u_color");
		glPatchParameteri(GL_PATCH_VERTICES, 3);
		
		glPointSize(5.0);
		glDrawArrays(GL_PATCHES, 0, 3);
		m_Shader->Unbind();
		
		m_VAO->Unbind();
	}
}