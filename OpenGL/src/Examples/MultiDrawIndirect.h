#pragma once
#include "VertexArray.h"
#include "VertexArrayBuffer.h"
#include "IndirectDrawBuffer.h"
#include "Shader.h"
#include "Tools/sbmMeshLoader.h"

namespace gl
{
	class MultiDrawIndirect
	{
	public:
		MultiDrawIndirect();
		~MultiDrawIndirect();

		void Draw(double currentTime, float aspect);

		void OnImGuiRender();

	private:
		void LoadShaders();

	private:
		struct DrawArraysIndirectCommand
		{
			GLuint  count;
			GLuint  primCount;
			GLuint  first;
			GLuint  baseInstance;
		};

		enum class MODE
		{
			MODE_MULTIDRAW = 0,
			MODE_SEPARATE_DRAWS
		};

		VertexArray* m_VAO;
		VertexArrayBuffer* m_VAB;
		IndirectDrawBuffer* m_IDB;
		Shader* m_Shader;
		sbmMesh m_Object;
		DrawArraysIndirectCommand* m_Command;

		MODE m_Mode = MODE::MODE_MULTIDRAW;
		bool m_Paused = false;
		bool m_VSync = false;
	};
}
