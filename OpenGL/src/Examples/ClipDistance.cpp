#include "ClipDistance.h"

#include <glm/gtc/matrix_transform.hpp>
#include <imgui.h>

namespace gl
{
	ClipDistance::ClipDistance()
	{
		m_Mesh.load("assets/meshes/dragon.sbm");
		m_Shader = new Shader("assets/shaders/ClipDistance.glsl");
	}

	ClipDistance::~ClipDistance()
	{
		delete m_Shader;
	}

	void ClipDistance::Draw(double currentTime, float aspect)
	{
        static const GLfloat black[] = { 0.0f, 0.0f, 0.0f, 0.0f };
        static const GLfloat one = 1.0f;

        static double last_time = 0.0;
        static double total_time = 0.0;

        if (!m_Paused)
            total_time += (currentTime - last_time);
        last_time = currentTime;

        float f = (float)total_time;

        glClearBufferfv(GL_COLOR, 0, black);
        glClearBufferfv(GL_DEPTH, 0, &one);

        m_Shader->Bind();

        glm::mat4 proj_matrix = glm::perspective(55.0f,
            aspect,
            0.1f,
            1000.0f);

        glm::mat4 mv_matrix = 
            glm::translate(glm::mat4(1.0f), glm::vec3(0.0f, 0.0f, -15.0f)) *
            glm::rotate(glm::mat4(1.0f),glm::radians(f * 0.34f), glm::vec3(0.0f, 1.0f, 0.0f)) *
            glm::translate(glm::mat4(1.0f), glm::vec3(0.0f, -4.0f, 0.0f));

        glm::mat4 plane_matrix = 
            glm::rotate(glm::mat4(1.0f), glm::radians(f * 6.0f), glm::vec3(1.0f, 0.0f, 0.0f)) *
            glm::rotate(glm::mat4(1.0f), glm::radians(f * 7.3f), glm::vec3(0.0f, 1.0f, 0.0f));

        glm::vec4 plane = plane_matrix[0];
        plane[3] = 0.0f;
        plane = glm::normalize(plane);

        glm::vec4 clip_sphere = glm::vec4(sinf(f * 0.7f) * 3.0f, cosf(f * 1.9f) * 3.0f, sinf(f * 0.1f) * 3.0f, cosf(f * 1.7f) + 2.5f);

        m_Shader->UploadUniformMat4(proj_matrix, "proj_matrix");
        m_Shader->UploadUniformMat4(mv_matrix, "mv_matrix");
        m_Shader->UploadUniformFloat4(plane, "clip_plane");
        m_Shader->UploadUniformFloat4(clip_sphere, "clip_sphere");

        glEnable(GL_DEPTH_TEST);
        glEnable(GL_CLIP_DISTANCE0);
        glEnable(GL_CLIP_DISTANCE1);
        glClipControl(GL_UPPER_LEFT, GL_ZERO_TO_ONE);

        m_Mesh.render();

        glDisable(GL_DEPTH_TEST);
        glDisable(GL_CLIP_DISTANCE0);
        glDisable(GL_CLIP_DISTANCE1);
        glClipControl(GL_LOWER_LEFT, GL_NEGATIVE_ONE_TO_ONE);
	}
    void ClipDistance::OnImGuiRender()
    {
        static char* pauseBtnLabel = "Pause";
        ImGui::Begin("Settings");
        {
            if (ImGui::Button(pauseBtnLabel)) {
                m_Paused = !m_Paused;
                m_Paused ? pauseBtnLabel = "UnPause" : pauseBtnLabel = "Pause";
            }
        }
        ImGui::End();
    }
}