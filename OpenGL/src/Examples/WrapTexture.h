#pragma once
#include "VertexArray.h"
#include "VertexArrayBuffer.h"
#include "Texture2D.h"
#include "Shader.h"

namespace gl
{
	class WrapTexture
	{
	public:
		WrapTexture();
		~WrapTexture();

		void Draw(double currentTime, float aspect);
	protected:
		void RendererInit();
		void RendererEnd();
	private:
		VertexArray* m_VAO;
		VertexArrayBuffer* m_VAB;
		Shader* m_Shader;
		Texture2D* m_Texture2D;
	};
}
