#include "SpinningCube.h"

#include <glm/gtc/matrix_transform.hpp>

#define _USE_MATH_DEFINES
#include <math.h>

namespace gl
{

	SpinningCube::SpinningCube()
	{
		m_Shader = new Shader("assets/shaders/cube.glsl");

		static const GLfloat vertex_positions[] =
		{
			-0.25f,  0.25f, -0.25f,
			-0.25f, -0.25f, -0.25f,
			 0.25f, -0.25f, -0.25f,

			 0.25f, -0.25f, -0.25f,
			 0.25f,  0.25f, -0.25f,
			-0.25f,  0.25f, -0.25f,

			 0.25f, -0.25f, -0.25f,
			 0.25f, -0.25f,  0.25f,
			 0.25f,  0.25f, -0.25f,

			 0.25f, -0.25f,  0.25f,
			 0.25f,  0.25f,  0.25f,
			 0.25f,  0.25f, -0.25f,

			 0.25f, -0.25f,  0.25f,
			-0.25f, -0.25f,  0.25f,
			 0.25f,  0.25f,  0.25f,

			-0.25f, -0.25f,  0.25f,
			-0.25f,  0.25f,  0.25f,
			 0.25f,  0.25f,  0.25f,

			-0.25f, -0.25f,  0.25f,
			-0.25f, -0.25f, -0.25f,
			-0.25f,  0.25f,  0.25f,

			-0.25f, -0.25f, -0.25f,
			-0.25f,  0.25f, -0.25f,
			-0.25f,  0.25f,  0.25f,

			-0.25f, -0.25f,  0.25f,
			 0.25f, -0.25f,  0.25f,
			 0.25f, -0.25f, -0.25f,

			 0.25f, -0.25f, -0.25f,
			-0.25f, -0.25f, -0.25f,
			-0.25f, -0.25f,  0.25f,

			-0.25f,  0.25f, -0.25f,
			 0.25f,  0.25f, -0.25f,
			 0.25f,  0.25f,  0.25f,

			 0.25f,  0.25f,  0.25f,
			-0.25f,  0.25f,  0.25f,
			-0.25f,  0.25f, -0.25f
		};


		m_VAO = new VertexArray;
		m_VAO->Bind();
		m_VAB = new VertexArrayBuffer(sizeof(vertex_positions));
		m_VAB->SetLayout({
			{ BufferDataType::Float3, "position" }
			});
		m_VAB->SetData(vertex_positions, sizeof(vertex_positions));
		m_VAO->AddBuffer(m_VAB);
		m_VAO->Unbind();
	}

	SpinningCube::~SpinningCube()
	{
		delete m_VAO;
		delete m_VAB;
		delete m_Shader;
	}

	void SpinningCube::Draw(double currentTime, float aspect)
	{
		RendererInit();
		m_VAO->Bind();
		glm::mat4 projection_matrix = glm::perspective(50.0f, aspect, 0.1f, 1000.0f);

		static const GLfloat one = 1.0f;
		glClearBufferfv(GL_DEPTH, 0, &one);

		m_Shader->Bind();
		m_Shader->UploadUniformMat4(projection_matrix, "proj_matrix");
#if 0
		float f = (float)currentTime * (float)M_PI * 0.1f;

		glm::mat4 mv_matrix = glm::translate(glm::mat4(1.0f), { 0.0f, 0.0f, -4.0f })
			* glm::translate(glm::mat4(1.0f), { sinf(2.1f * f) * 0.5f, cosf(1.7f * f) * 0.5f, sinf(1.3f * f) * cosf(1.5f * f) * 2.0f })
			* glm::rotate(glm::mat4(1.0f), glm::radians((float)currentTime * 45.0f), { 0.0f, 1.0f, 0.0f })
			* glm::rotate(glm::mat4(1.0f), glm::radians((float)currentTime * 81.0f), { 1.0f, 0.0f, 0.0f });

		// replicate with glm
		glm::mat4 view_matrix = (glm::mat4&)mv_matrix;

		m_Shader->UploadUniformMat4(view_matrix, "mv_matrix");
		m_Shader->UploadUniformMat4(projection_matrix, "proj_matrix");
		glDrawArrays(GL_TRIANGLES, 0, 36);
#else
		for (int i = 0; i < 24; i++)
		{
			float f = (float)i + (float)currentTime * (float)M_PI * 0.1f;

			glm::mat4 mv_matrix = glm::translate(glm::mat4(1.0f), { 0.0f, 0.0f, -4.0f })
				* glm::translate(glm::mat4(1.0f), { sinf(2.1f * f) * 0.5f, cosf(1.7f * f) * 0.5f, sinf(1.3f * f) * cosf(1.5f * f) * 2.0f })
				* glm::rotate(glm::mat4(1.0f), glm::radians((float)currentTime * 45.0f), { 0.0f, 1.0f, 0.0f })
				* glm::rotate(glm::mat4(1.0f), glm::radians((float)currentTime * 81.0f), { 1.0f, 0.0f, 0.0f })
				* glm::scale(glm::mat4(1.0f), glm::vec3(0.1));


			glm::mat4 view_matrix = (glm::mat4&)mv_matrix;
			m_Shader->UploadUniformMat4(view_matrix, "mv_matrix");
			glDrawArrays(GL_TRIANGLES, 0, 36);
		}
#endif
		RendererEnd();
	}

	void SpinningCube::RendererInit()
	{
		glEnable(GL_CULL_FACE);
		glFrontFace(GL_CW);

		glEnable(GL_DEPTH_TEST);
		glDepthFunc(GL_LEQUAL);
	}
	void SpinningCube::RendererEnd()
	{
		glDisable(GL_CULL_FACE);
		glDisable(GL_DEPTH_TEST);
	}
}