#include "TessModes.h"

#include <imgui.h>

#include <iostream>

namespace gl
{
	TessModes::TessModes()
	{
		m_QuadShader = new Shader("assets/shaders/TessModes_Quads.glsl");
		m_TriShader = new Shader("assets/shaders/TessModes_Triangle.glsl");
		m_IsoShader = new Shader("assets/shaders/TessModes_Isoline.glsl");
		m_VAO = new VertexArray;
	}

	TessModes::~TessModes()
	{
		delete m_QuadShader;
		delete m_VAO;
	}

	void TessModes::Draw()
	{
		glPatchParameteri(GL_PATCH_VERTICES, 4);
		if (m_Mode == Mode::QUADS)
			m_QuadShader->Bind();
		else if (m_Mode == Mode::TRIANGLE)
			m_TriShader->Bind();
		else if (m_Mode == Mode::ISOLINES)
			m_IsoShader->Bind();
		
		m_VAO->Bind();

		glPolygonMode(GL_FRONT_AND_BACK, GL_LINE);
		glDrawArrays(GL_PATCHES, 0, 4);
		glPolygonMode(GL_FRONT_AND_BACK, GL_FILL);

	}

	void TessModes::OnImGuiRender()
	{
		static const char* items[] = { "Quads", "Triangles", "Isolines" };
		static const char* current_item = items[0];
		ImGui::Begin("Settings");
		{
			if (ImGui::BeginCombo("##combo", current_item))
			{
				for (int n = 0; n < IM_ARRAYSIZE(items); n++)
				{
					bool is_selected = (current_item == items[n]);
					if (ImGui::Selectable(items[n], is_selected)) {
						current_item = items[n];
						m_Mode = (Mode)n;
					}
					if (is_selected) {
						ImGui::SetItemDefaultFocus();
					}
				}
				ImGui::EndCombo();
			}
		}
		ImGui::End();
	}
}