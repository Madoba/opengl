#include "FragmentList.h"

#include "Events/KeyEvent.h"
#include "Events/Input.h"

#include <glm/gtc/matrix_transform.hpp>
#include <glm/gtc/type_ptr.hpp>

#include "Tools/vmath.h"

#include <imgui.h>

#include <iostream>

namespace gl
{
    // TODO: Explore more this concept and find the bug
	FragmentList::FragmentList()
	{
		m_ClearShader = new Shader("assets/shaders/FragmentList_Clear.glsl");
		m_AppendShader = new Shader("assets/shaders/FragmentList_Append.glsl");
		m_ResolveShader = new Shader("assets/shaders/FragmentList_Resolve.glsl");

        glGenBuffers(1, &uniforms_buffer);
        glBindBuffer(GL_UNIFORM_BUFFER, uniforms_buffer);
        glBufferData(GL_UNIFORM_BUFFER, sizeof(uniforms_block), NULL, GL_DYNAMIC_DRAW);

        m_DragonMesh.load("assets/meshes/dragon.sbm");

		glGenBuffers(1, &fragment_buffer);
		glBindBuffer(GL_SHADER_STORAGE_BUFFER, fragment_buffer);
		glBufferData(GL_SHADER_STORAGE_BUFFER, 1024 * 1024 * 16, NULL, GL_DYNAMIC_COPY);

		glGenBuffers(1, &atomic_counter_buffer);
		glBindBuffer(GL_ATOMIC_COUNTER_BUFFER, atomic_counter_buffer);
		glBufferData(GL_ATOMIC_COUNTER_BUFFER, 4, NULL, GL_DYNAMIC_COPY);

        glActiveTexture(GL_TEXTURE0);
		glGenTextures(1, &head_pointer_image);
		glBindTexture(GL_TEXTURE_2D, head_pointer_image);
		glTexStorage2D(GL_TEXTURE_2D, 1, GL_R32UI, 1024, 1024);

		glGenVertexArrays(1, &dummy_vao);
        glBindVertexArray(dummy_vao);
	}

	FragmentList::~FragmentList()
	{
		delete m_ClearShader;
		delete m_AppendShader;
		delete m_ResolveShader;
	}

	void FragmentList::Draw(double currentTime, float aspect)
	{
        glClipControl(GL_UPPER_LEFT, GL_ZERO_TO_ONE);

        const float f = (float)currentTime;

        glMemoryBarrier(GL_SHADER_IMAGE_ACCESS_BARRIER_BIT | GL_ATOMIC_COUNTER_BARRIER_BIT | GL_SHADER_STORAGE_BARRIER_BIT);

        m_ClearShader->Bind();
        glBindVertexArray(dummy_vao);
        glDrawArrays(GL_TRIANGLE_STRIP, 0, 4);

        m_AppendShader->Bind();

        glm::mat4 model_matrix = glm::scale(glm::mat4(1.0f), glm::vec3(m_Scale));
        glm::vec3 view_position = glm::vec3(cos(f * 0.35f) * 120.0f, cos(f * 0.4f) * 30.0f, sin(f * 0.35f) * 120.0f);

        glm::mat4 view_matrix = glm::lookAt(view_position,
            glm::vec3(0.0f, 15.0f, 0.0f),
            glm::vec3(0.0f, 1.0f, 0.0f));

        glm::mat4 mv_matrix = view_matrix * model_matrix;
        glm::mat4 proj_matrix = glm::perspective(50.0f,
            aspect,
            0.1f,
            1000.0f);

        m_AppendShader->UploadUniformMat4(proj_matrix * mv_matrix, "mvp");

        static const unsigned int zero = 0;
        glBindBuffer(GL_ATOMIC_COUNTER_BUFFER, atomic_counter_buffer);
        glBindBufferBase(GL_ATOMIC_COUNTER_BUFFER, 0, atomic_counter_buffer);
        glBufferSubData(GL_ATOMIC_COUNTER_BUFFER, 0, sizeof(zero), &zero);
        
        glBindBuffer(GL_SHADER_STORAGE_BUFFER, fragment_buffer);
        glBindBufferBase(GL_SHADER_STORAGE_BUFFER, 0, fragment_buffer);

        glActiveTexture(GL_TEXTURE0);
        glBindTexture(GL_TEXTURE_2D, head_pointer_image);
        glBindImageTexture(0, head_pointer_image, 0, GL_FALSE, 0, GL_READ_WRITE, GL_R32UI);

        glMemoryBarrier(GL_SHADER_IMAGE_ACCESS_BARRIER_BIT | GL_ATOMIC_COUNTER_BARRIER_BIT | GL_SHADER_STORAGE_BARRIER_BIT);

        m_DragonMesh.render();

        glMemoryBarrier(GL_SHADER_IMAGE_ACCESS_BARRIER_BIT | GL_ATOMIC_COUNTER_BARRIER_BIT | GL_SHADER_STORAGE_BARRIER_BIT);

        m_ResolveShader->Bind();
        glBindVertexArray(dummy_vao);

        glMemoryBarrier(GL_SHADER_IMAGE_ACCESS_BARRIER_BIT | GL_ATOMIC_COUNTER_BARRIER_BIT | GL_SHADER_STORAGE_BARRIER_BIT);

        glDrawArrays(GL_TRIANGLE_STRIP, 0, 4);
        glClipControl(GL_LOWER_LEFT, GL_NEGATIVE_ONE_TO_ONE);
	}

    void FragmentList::OnImGuiRender()
    {
        ImGui::Begin("Scale");
        {
            ImGui::SliderFloat("Scale", &m_Scale, 0.0f, 100.0f);
        }
        ImGui::End();
    }

    void FragmentList::OnEvent(Event& e)
    {
        EventDispatcher dispatcher(e);
        dispatcher.Dispatch<KeyPressedEvent>([this](auto&&)
            {
                if (Input::IsKeyPressed(GL_KEY_R))
                {
                    delete m_ClearShader;
                    delete m_AppendShader;
                    delete m_ResolveShader;

                    m_ClearShader = new Shader("assets/shaders/FragmentList_Clear.glsl");
                    m_AppendShader = new Shader("assets/shaders/FragmentList_Append.glsl");
                    m_ResolveShader = new Shader("assets/shaders/FragmentList_Resolve.glsl");
                }
                return true;
            });
    }
}