#include "GeoShaderQuad.h"

#include <glm/gtc/matrix_transform.hpp>
#include <imgui.h>

namespace gl
{
	GeoShaderQuad::GeoShaderQuad()
	{
		m_VAO = new VertexArray;

		m_FanShader = new Shader("assets/shaders/GeoShaderQuadFans.glsl");
		m_LineAdjShader = new Shader("assets/shaders/GeoShaderQuadLineAdj.glsl");		
	}

	GeoShaderQuad::~GeoShaderQuad()
	{
		delete m_VAO;
		delete m_FanShader;
		delete m_LineAdjShader;
	}

	void GeoShaderQuad::Draw(double currentTime, float aspect)
	{
		static const GLfloat black[] = { 0.0f, 0.25f, 0.0f, 1.0f };
		static double last_time = 0.0;
		static double total_time = 0.0;

		if (!m_Paused)
			total_time += (currentTime - last_time);
		last_time = currentTime;

		glClearBufferfv(GL_COLOR, 0, black);

		glm::mat4 mv_matrix = glm::translate(glm::mat4(1.0f), glm::vec3(0.0f, 0.0f, -10.0f)) *
			glm::rotate(glm::mat4(1.0f), glm::radians((float)currentTime * 5.0f), glm::vec3(0.0f, 0.0f, 1.0f)) *
			glm::rotate(glm::mat4(1.0f), glm::radians((float)currentTime * 30.0f), glm::vec3(1.0f, 0.0f, 0.0f));

		glm::mat4 proj_matrix = glm::perspective(50.0f, aspect, 0.1f, 1000.0f);
		glm::mat4 mvp = proj_matrix * mv_matrix;
		m_VAO->Bind();
		switch (m_Mode)
		{
		case 0:
			m_FanShader->Bind();
			m_FanShader->UploadUniformMat4(mvp, "mvp");
			m_FanShader->UploadUniformInt(0, "vid_offset");
			glDrawArrays(GL_TRIANGLE_FAN, 0, 4);
			break;
		case 1:
			m_LineAdjShader->Bind();
			m_LineAdjShader->UploadUniformMat4(mvp, "mvp");
			m_LineAdjShader->UploadUniformInt(0, "vid_offset");
			glDrawArrays(GL_LINES_ADJACENCY, 0, 4);
			break;
		}
	}

	void GeoShaderQuad::OnImGuiRender()
	{
		static const char* items[] = { "TRIANGLE_FAN", "LINES_ADJACENCY" };
		static const char* current_item = items[0];
		ImGui::Begin("Geometry shader quads");
		{
			if (ImGui::BeginCombo("##combo", current_item))
			{
				for (int n = 0; n < IM_ARRAYSIZE(items); n++)
				{
					bool is_selected = (current_item == items[n]);
					if (ImGui::Selectable(items[n], is_selected)) {
						current_item = items[n];
						m_Mode = (Mode)n;
					}
					if (is_selected) {
						ImGui::SetItemDefaultFocus();
					}
				}
				ImGui::EndCombo();
			}
		}
		ImGui::End();
	}
}