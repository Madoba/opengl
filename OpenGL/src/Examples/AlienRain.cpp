#include "AlienRain.h"

namespace gl
{
	AlienRain::AlienRain()
	{
		m_Shader = new Shader("assets/shaders/AlienRain.glsl");
		m_Texture2DArray = new Texture2DArray("assets/textures/aliens.ktx");

		
		for (int i = 0; i < 256; i++)
		{
			droplet_x_offset[i] = static_cast <float> (rand()) / (static_cast <float> (RAND_MAX / 1)) * 2.0f - 1.0f;
			droplet_rot_speed[i] = (static_cast <float> (rand()) / (static_cast <float> (RAND_MAX / 1)) + 0.5f) * ((i & 1) ? -3.0f : 3.0f);
			droplet_fall_speed[i] = static_cast <float> (rand()) / (static_cast <float> (RAND_MAX / 1)) + 0.2f;
		}

		m_VAO = new VertexArray;
		m_UB = new UniformBuffer(256 * sizeof(glm::vec4));
	}

	AlienRain::~AlienRain()
	{
		delete m_VAO;
		delete m_UB;
		delete m_Shader;
		delete m_Texture2DArray;
	}

	void AlienRain::Draw(double currentTime)
	{
		glEnable(GL_BLEND);
		glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);

		m_Texture2DArray->Bind();
		m_Shader->Bind();
		m_VAO->Bind();
		m_UB->Bind();
		glm::vec4* droplet = (glm::vec4*)m_UB->GetMapBuffer(256 * sizeof(glm::vec4));
		for (int i = 0; i < 256; i++)
		{
			droplet[i][0] = droplet_x_offset[i];
			droplet[i][1] = 2.0f - fmodf((currentTime + float(i)) * droplet_fall_speed[i], 4.31f);
			droplet[i][2] = currentTime * droplet_rot_speed[i];
			droplet[i][3] = 0.0f;
		}
		m_UB->Unbind();
		int alien_index;
		for (alien_index = 0; alien_index < 256; alien_index++)
		{
			glVertexAttribI1i(0, alien_index);
			glDrawArrays(GL_TRIANGLE_STRIP, 0, 4);
		}
		glDisable(GL_BLEND);
	}
}