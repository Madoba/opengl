#pragma once
#include "Shader.h"
#include "Tools/sbmMeshLoader.h"

namespace gl
{
	class GeomShaderCulling
	{
	public:
		GeomShaderCulling();
		~GeomShaderCulling();

		void Draw(double currentTime, float aspect);

	private:
		Shader* m_Shader;
		sbmMesh m_Mesh;
	};
}
