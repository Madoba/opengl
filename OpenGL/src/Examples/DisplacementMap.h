#pragma once
#include "Shader.h"
#include "VertexArray.h"
#include "Texture2D.h"

namespace gl
{
	class DisplacementMap
	{
	public:
		DisplacementMap();
		~DisplacementMap();

		void Draw(double currentTime, float aspect);
		void OnImGuiRender();
	private:
		Shader* m_Shader;
		VertexArray* m_VAO;
		Texture2D* m_Terragen;
		Texture2D* m_TerragenColor;

		bool m_Paused = false;
		bool m_Displacement = true;
		float m_MapDepth = 6.0f;
		bool m_Fog = false;

		bool m_Wireframe = false;
	};

}
