#include "DisplacementMap.h"

#include <glm/gtc/matrix_transform.hpp>

#include <imgui.h>

#include "Tools/KTXLoader.h"

namespace gl
{
	DisplacementMap::DisplacementMap()
	{
		m_Shader = new Shader("assets/shaders/DisplacementMap.glsl");
		m_VAO = new VertexArray;
		m_VAO->Bind();

		glActiveTexture(GL_TEXTURE5);
		m_Terragen = new Texture2D("assets/textures/terragen1.ktx");
		glActiveTexture(GL_TEXTURE6);
		m_TerragenColor = new Texture2D("assets/textures/terragen_color.ktx");

	}

	DisplacementMap::~DisplacementMap()
	{
		delete m_Shader;
		delete m_VAO;
		delete m_Terragen;
		delete m_TerragenColor;
	}

	void DisplacementMap::Draw(double currentTime, float aspect)
	{
		static const GLfloat black[] = { 0.85f, 0.95f, 1.0f, 1.0f };
		static const GLfloat one = 1.0f;
		static double last_time = 0.0;
		static double total_time = 0.0;

		m_VAO->Bind();
		glPatchParameteri(GL_PATCH_VERTICES, 4);
		glEnable(GL_CULL_FACE);

		if (!m_Paused)
			total_time += (currentTime - last_time);
		last_time = currentTime;

		float t = (float)total_time * 0.03f;
		float r = sinf(t * 5.37f) * 15.0f + 16.0f;
		float h = cosf(t * 4.79f) * 2.0f + 3.2f;

		glClearBufferfv(GL_COLOR, 0, black);
		glClearBufferfv(GL_DEPTH, 0, &one);

		glm::mat4 mv_matrix =  
			/*glm::translate(glm::mat4(1.0f), glm::vec3(0.0f, 0.0f, -1.4f)) *
			glm::translate(glm::mat4(1.0f), glm::vec3(0.0f, -0.4f, 0.0f)) *
			glm::rotate(glm::mat4(1.0f), glm::radians((float)currentTime * 6.0f), glm::vec3(0.0f, 1.0f, 0.0f)) **/
			glm::lookAt(glm::vec3(sinf(t) * r, h, cosf(t) * r), glm::vec3(0.0f), glm::vec3(0.0f, 1.0f, 0.0f));

		glm::mat4 proj_matrix = glm::perspective(
			55.0f,
			aspect,
			0.1f, 1000.0f);

		m_Shader->Bind();
		m_Shader->UploadUniformMat4(mv_matrix, "mv_matrix");
		m_Shader->UploadUniformMat4(proj_matrix, "proj_matrix");
		m_Shader->UploadUniformMat4(proj_matrix * mv_matrix, "mvp_matrix");
		m_Shader->UploadUniformFloat(m_Displacement ? m_MapDepth : 0.0f, "dmap_depth");
		m_Shader->UploadUniformInt(m_Fog ? 1 : 0, "enable_fog");

		glEnable(GL_DEPTH_TEST);
		glDepthFunc(GL_LEQUAL);
		glClipControl(GL_UPPER_LEFT, GL_ZERO_TO_ONE);

		if (m_Wireframe)
			glPolygonMode(GL_FRONT_AND_BACK, GL_LINE);
		else
			glPolygonMode(GL_FRONT_AND_BACK, GL_FILL);
		glDrawArraysInstanced(GL_PATCHES, 0, 4, 64 * 64);

		glDisable(GL_DEPTH_TEST);
		glDisable(GL_CULL_FACE);
		glClipControl(GL_LOWER_LEFT, GL_NEGATIVE_ONE_TO_ONE);
		glPolygonMode(GL_FRONT_AND_BACK, GL_FILL);
	}

	void DisplacementMap::OnImGuiRender()
	{
		static char* pauseBtnLabel = "Pause";
		static char* modeBtnLabel = "Wireframe";
		static char* fogBtnLabel = "Fog - On";
		ImGui::Begin("Settings");
		{
			if (ImGui::Button(pauseBtnLabel)) {
				m_Paused = !m_Paused;
				m_Paused ? pauseBtnLabel = "UnPause" : pauseBtnLabel = "Pause";
			}
			ImGui::Checkbox("Wireframe", &m_Wireframe);
			ImGui::Checkbox("Fog", &m_Fog);
			ImGui::Checkbox("Displacement", &m_Displacement);
			ImGui::SliderFloat("Map depth", &m_MapDepth, 0.0f, 100.0f);
		}
		ImGui::End();
	}
}