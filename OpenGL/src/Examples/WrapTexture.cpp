#include "WrapTexture.h"

#include <glm/gtc/matrix_transform.hpp>

#define _USE_MATH_DEFINES
#include <math.h>

namespace gl
{

	WrapTexture::WrapTexture()
	{
		m_Shader = new Shader("assets/shaders/WrapTexture.glsl");
		m_Texture2D = new Texture2D("assets/textures/rocks.ktx");

		static const GLfloat vertex_positions[] =
		{
			-0.25f,  0.25f, -0.25f,
			-0.25f, -0.25f, -0.25f,
			 0.25f, -0.25f, -0.25f,

			 0.25f, -0.25f, -0.25f,
			 0.25f,  0.25f, -0.25f,
			-0.25f,  0.25f, -0.25f,

			 0.25f, -0.25f, -0.25f,
			 0.25f, -0.25f,  0.25f,
			 0.25f,  0.25f, -0.25f,

			 0.25f, -0.25f,  0.25f,
			 0.25f,  0.25f,  0.25f,
			 0.25f,  0.25f, -0.25f,

			 0.25f, -0.25f,  0.25f,
			-0.25f, -0.25f,  0.25f,
			 0.25f,  0.25f,  0.25f,

			-0.25f, -0.25f,  0.25f,
			-0.25f,  0.25f,  0.25f,
			 0.25f,  0.25f,  0.25f,

			-0.25f, -0.25f,  0.25f,
			-0.25f, -0.25f, -0.25f,
			-0.25f,  0.25f,  0.25f,

			-0.25f, -0.25f, -0.25f,
			-0.25f,  0.25f, -0.25f,
			-0.25f,  0.25f,  0.25f,

			-0.25f, -0.25f,  0.25f,
			 0.25f, -0.25f,  0.25f,
			 0.25f, -0.25f, -0.25f,

			 0.25f, -0.25f, -0.25f,
			-0.25f, -0.25f, -0.25f,
			-0.25f, -0.25f,  0.25f,

			-0.25f,  0.25f, -0.25f,
			 0.25f,  0.25f, -0.25f,
			 0.25f,  0.25f,  0.25f,

			 0.25f,  0.25f,  0.25f,
			-0.25f,  0.25f,  0.25f,
			-0.25f,  0.25f, -0.25f
		};


		m_VAO = new VertexArray;
		m_VAO->Bind();
		m_VAB = new VertexArrayBuffer(sizeof(vertex_positions));
		m_VAB->SetLayout({
			{ BufferDataType::Float3, "v_Position" }
			});
		m_VAB->SetData(vertex_positions, sizeof(vertex_positions));
		m_VAO->AddBuffer(m_VAB);
		m_VAO->Unbind();
	}

	WrapTexture::~WrapTexture()
	{
		delete m_VAO;
		delete m_VAB;
		delete m_Shader;
		delete m_Texture2D;
	}

	void WrapTexture::Draw(double currentTime, float aspect)
	{
		RendererInit();
		m_VAO->Bind();
		glm::mat4 projection_matrix = glm::perspective(50.0f, aspect, 0.1f, 1000.0f);

		static const GLfloat one = 1.5f;
		glClearBufferfv(GL_DEPTH, 0, &one);

		m_Texture2D->Bind();
		m_Shader->Bind();
		m_Shader->UploadUniformMat4(projection_matrix, "proj_matrix");
		float f = (float)currentTime * (float)M_PI * 0.1f;

		glm::mat4 mv_matrix = glm::translate(glm::mat4(1.0f), { 0.0f, 0.0f, -4.0f })
			* glm::translate(glm::mat4(1.0f), { sinf(2.1f * f) * 0.5f, cosf(1.7f * f) * 0.5f, sinf(1.3f * f) * cosf(1.5f * f) * 2.0f })
			* glm::rotate(glm::mat4(1.0f), glm::radians((float)currentTime * 45.0f), { 0.0f, 1.0f, 0.0f })
			* glm::rotate(glm::mat4(1.0f), glm::radians((float)currentTime * 81.0f), { 1.0f, 0.0f, 0.0f })
			* glm::scale(glm::mat4(1.0f), glm::vec3(0.5));

		// replicate with glm
		glm::mat4 view_matrix = (glm::mat4&)mv_matrix;

		m_Shader->UploadUniformMat4(view_matrix, "mv_matrix");
		m_Shader->UploadUniformMat4(projection_matrix, "proj_matrix");
		glDrawArrays(GL_TRIANGLES, 0, 36);
		m_Texture2D->Unbind();
		m_Shader->Unbind();
		m_VAO->Unbind();
		RendererEnd();
	}

	void WrapTexture::RendererInit()
	{
		glEnable(GL_CULL_FACE);
		glFrontFace(GL_CW);

		glEnable(GL_DEPTH_TEST);
		glDepthFunc(GL_LEQUAL);
	}

	void WrapTexture::RendererEnd()
	{
		glDisable(GL_CULL_FACE);
		glDisable(GL_DEPTH_TEST);
	}
}