#pragma once
#include "VertexArray.h"
#include "VertexArrayBuffer.h"
#include "Texture2D.h"
#include "Shader.h"

#include <vector>

namespace gl
{
	class Tunnel
	{
	public:
		Tunnel();
		~Tunnel();

		void Draw(double currentTime, float aspect);

	private:
		VertexArray* m_VAO;
		VertexArrayBuffer* m_VAB;
		Shader* m_Shader;
		Texture2D* m_TexWall;
		Texture2D* m_TexFloor;
		Texture2D* m_TexCeiling;
	};
}
