#pragma once
#include "VertexArray.h"
#include "VertexArrayBuffer.h"
#include "Shader.h"

namespace gl
{
	class InstancedAttributes
	{
	public:
		InstancedAttributes();
		~InstancedAttributes();

		void Draw();

	private:
		VertexArray* m_VAO;
		VertexArrayBuffer* m_VAB;
		Shader* m_Shader;

		GLuint      square_buffer;
		GLuint      square_vao;
	};
}
