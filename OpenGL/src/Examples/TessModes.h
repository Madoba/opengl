#pragma once

#include "Shader.h"
#include "VertexArray.h"

namespace gl
{
	class TessModes
	{
	public:
		enum class Mode
		{
			QUADS = 0,
			TRIANGLE,
			ISOLINES
		};
	public:
		TessModes();
		~TessModes();

		void Draw();
		void OnImGuiRender();
	private:
		Shader* m_QuadShader;
		Shader* m_TriShader;
		Shader* m_IsoShader;
		VertexArray* m_VAO;
		Mode m_Mode = Mode::QUADS;
	};

}
