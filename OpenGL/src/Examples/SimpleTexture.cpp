#include "SimpleTexture.h"

namespace gl
{
	SimpleTexture::SimpleTexture()
	{
		glActiveTexture(GL_TEXTURE0);
		glCreateTextures(GL_TEXTURE_2D, 1, &m_TextureID);
		glBindTexture(GL_TEXTURE_2D, m_TextureID);
		// Specify the amount of storage we want to use for the texture
		glTextureStorage2D(m_TextureID, 1, GL_RGBA32F, 256, 256);

		// Manually creating texture
		float* data = new float[256 * 256 * 4];
		generate_texture(data, 256, 256);
		glTextureSubImage2D(m_TextureID, 0, 0, 0, 256, 256, GL_RGBA, GL_FLOAT, data);
		delete[] data;

		m_Shader = new Shader("assets/shaders/simpleTexture.glsl");

		static const float vertices[] = {	 0.75, -0.75, 0.5,
											-0.75, -0.75, 0.5,
											 0.75,  0.75, 0.5 };
		m_VAO = new VertexArray;
		m_VAO->Bind();
		m_VAB = new VertexArrayBuffer(sizeof(vertices));
		m_VAB->SetLayout({
			{ BufferDataType::Float3, "position" }
			});
		m_VAB->SetData(vertices, sizeof(vertices));
		m_VAO->AddBuffer(m_VAB);
		m_VAO->Unbind();
	}

	SimpleTexture::~SimpleTexture()
	{
		delete m_VAO;
		delete m_VAB;
		delete m_Shader;
	}

	void SimpleTexture::Draw()
	{
		// if GL_DEPTH_TEST is enable on other examples must be disable in order to draw2D textures
		// TODO: abstract scenes to draw 2D and 3D
		glActiveTexture(GL_TEXTURE0);
		glTextureParameteri(m_TextureID, GL_TEXTURE_MIN_FILTER, GL_NEAREST);
		glTextureParameteri(m_TextureID, GL_TEXTURE_MAG_FILTER, GL_NEAREST);

		glTextureParameteri(m_TextureID, GL_TEXTURE_WRAP_S, GL_REPEAT);
		glTextureParameteri(m_TextureID, GL_TEXTURE_WRAP_T, GL_REPEAT);


		glBindTexture(GL_TEXTURE_2D, m_TextureID);
		m_Shader->Bind();
		m_VAO->Bind();
		glDrawArrays(GL_TRIANGLES, 0, 3);
		m_VAO->Unbind();
		m_Shader->Unbind();
		glBindTexture(GL_TEXTURE_2D, 0);
	}

	void SimpleTexture::generate_texture(float* data, uint32_t width, uint32_t height)
	{
		int x, y;

		for (y = 0; y < height; y++)
		{
			for (x = 0; x < width; x++)
			{
				data[(y * width + x) * 4 + 0] = (float)((x & y) & 0xFF) / 255.0f;
				data[(y * width + x) * 4 + 1] = (float)((x | y) & 0xFF) / 255.0f;
				data[(y * width + x) * 4 + 2] = (float)((x ^ y) & 0xFF) / 255.0f;
				data[(y * width + x) * 4 + 3] = 1.0f;
			}
		}
	}
}