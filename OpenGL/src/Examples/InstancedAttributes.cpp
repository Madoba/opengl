#include "InstancedAttributes.h"

#include <glm/glm.hpp>

#include <iostream>

namespace gl
{
	struct attributes
	{
		glm::mat4 square_vertices;
		glm::mat4 instance_colors;
		glm::mat4 instance_positions;
	};


	InstancedAttributes::InstancedAttributes()
	{
		m_Shader = new Shader("assets/shaders/InstancedAttributes.glsl");

		attributes att; 
		att.square_vertices =
		{
			-1.0f, -1.0f, 0.0f, 1.0f,
			 1.0f, -1.0f, 0.0f, 1.0f,
			 1.0f,  1.0f, 0.0f, 1.0f,
			-1.0f,  1.0f, 0.0f, 1.0f
		};

		att.instance_colors = 
		{
			1.0f, 0.0f, 0.0f, 1.0f,
			0.0f, 1.0f, 0.0f, 1.0f,
			0.0f, 0.0f, 1.0f, 1.0f,
			1.0f, 1.0f, 0.0f, 1.0f
		};

		att.instance_positions =
		{
			-2.0f, -2.0f, 0.0f, 0.0f,
			 2.0f, -2.0f, 0.0f, 0.0f,
			 2.0f,  2.0f, 0.0f, 0.0f,
			-2.0f,  2.0f, 0.0f, 0.0f
		};

		m_VAO = new VertexArray;
		m_VAO->Bind();
		m_VAB = new VertexArrayBuffer(sizeof(att));
		m_VAB->SetLayout({
			{ BufferDataType::Mat4, "position" },
			{ BufferDataType::Mat4, "instance_color" },
			{ BufferDataType::Mat4, "instance_position" }
			});
		m_VAB->SetData((void*)&att, sizeof(att));
		m_VAO->AddInstancedBuffer(m_VAB);

		glVertexAttribDivisor(1, 1);
		glVertexAttribDivisor(2, 1);
	}

	InstancedAttributes::~InstancedAttributes()
	{
		delete m_Shader;
		delete m_VAO;
	}

	void InstancedAttributes::Draw()
	{
		static const GLfloat black[] = { 0.0f, 0.0f, 0.0f, 0.0f };
		glClearBufferfv(GL_COLOR, 0, black);

		m_Shader->Bind();
		m_VAO->Bind();
		glDrawArraysInstanced(GL_TRIANGLE_FAN, 0, 4, 4);
	}
}