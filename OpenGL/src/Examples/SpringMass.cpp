#include "SpringMass.h"

#include <glm/glm.hpp>
#include <imgui.h>

namespace gl
{
	SpringMass::SpringMass()
	{
		LoadShader();

		glm::vec4* initial_positions = new glm::vec4[points_total];
		glm::vec3* initial_velocities = new glm::vec3[points_total];
		glm::ivec4* connection_vectors = new glm::ivec4[points_total];

		int n = 0;
		for (int j = 0; j < points_y; j++) {
			float fj = (float)j / (float)points_y;
			for (int i = 0; i < points_x; i++) {
				float fi = (float)i / (float)points_x;

				initial_positions[n] = glm::vec4(0.0f, 0.0f, 0.0f, 0.0f);

				initial_positions[n] = glm::vec4((fi - 0.5f) * (float)points_x,
					(fj - 0.5f) * (float)points_y,
					0.6f * sinf(fi) * cosf(fj),
					1.0f);
				initial_velocities[n] = glm::vec3(0.0f);

				connection_vectors[n] = glm::ivec4(-1);

				if (j != (points_y - 1))
				{
					if (i != 0)
						connection_vectors[n][0] = n - 1;

					if (j != 0)
						connection_vectors[n][1] = n - points_x;

					if (i != (points_x - 1))
						connection_vectors[n][2] = n + 1;

					if (j != (points_y - 1))
						connection_vectors[n][3] = n + points_x;
				}
				n++;
			}
		}

		// TODO: abstract
		glGenVertexArrays(2, m_VAO);
		glGenBuffers(5, m_VBO);

		for (int i = 0; i < 2; i++) {
			glBindVertexArray(m_VAO[i]);

			glBindBuffer(GL_ARRAY_BUFFER, m_VBO[BUFFER_TYPE::POSITION_A + i]);
			glBufferData(GL_ARRAY_BUFFER, points_total * sizeof(glm::vec4),initial_positions, GL_DYNAMIC_COPY);
			glVertexAttribPointer(0, 4, GL_FLOAT, GL_FALSE, 0, NULL);
			glEnableVertexAttribArray(0);

			glBindBuffer(GL_ARRAY_BUFFER, m_VBO[BUFFER_TYPE::VELOCITY_A + i]);
			glBufferData(GL_ARRAY_BUFFER, points_total * sizeof(glm::vec3),initial_velocities, GL_DYNAMIC_COPY);
			glVertexAttribPointer(1, 3, GL_FLOAT, GL_FALSE, 0, NULL);
			glEnableVertexAttribArray(1);

			glBindBuffer(GL_ARRAY_BUFFER, m_VBO[BUFFER_TYPE::CONNECTION]);
			glBufferData(GL_ARRAY_BUFFER, points_total * sizeof(glm::ivec4),connection_vectors, GL_STATIC_DRAW);
			glVertexAttribIPointer(2, 4, GL_INT, 0, NULL);
			glEnableVertexAttribArray(2);
		}

		delete initial_positions;
		delete initial_velocities;
		delete connection_vectors;

		glGenTextures(2, m_TexturePos);
		glBindTexture(GL_TEXTURE_BUFFER, m_TexturePos[0]);
		glTexBuffer(GL_TEXTURE_BUFFER, GL_RGBA32F, m_VBO[BUFFER_TYPE::POSITION_A]);
		glBindTexture(GL_TEXTURE_BUFFER, m_TexturePos[1]);
		glTexBuffer(GL_TEXTURE_BUFFER, GL_RGBA32F, m_VBO[BUFFER_TYPE::POSITION_B]);

		int lines = (points_x - 1) * points_y + (points_y - 1) * points_x;

		glGenBuffers(1, &m_VIB);
		glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, m_VIB);
		glBufferData(GL_ELEMENT_ARRAY_BUFFER, lines * 2 * sizeof(int), NULL,GL_DYNAMIC_DRAW);

		int* e = (int*)glMapBufferRange(GL_ELEMENT_ARRAY_BUFFER, 0, lines * 2 * sizeof(int), GL_MAP_WRITE_BIT | GL_MAP_INVALIDATE_BUFFER_BIT);

		for (int j = 0; j < points_y; j++)
		{
			for (int i = 0; i < points_x - 1; i++)
			{
				*e++ = i + j * points_x;
				*e++ = 1 + i + j * points_x;
			}
		}

		for (int i = 0; i < points_x; i++)
		{
			for (int j = 0; j < points_y - 1; j++)
			{
				*e++ = i + j * points_x;
				*e++ = points_x + i + j * points_x;
			}
		}

		glUnmapBuffer(GL_ELEMENT_ARRAY_BUFFER);
	}

	SpringMass::~SpringMass()
	{
		glDeleteBuffers(5, m_VBO);
		glDeleteVertexArrays(2, m_VAO);
		delete m_ShaderUpdate;
		delete m_ShaderRender;
	}

	void SpringMass::Draw()
	{
		m_ShaderUpdate->Bind();

		glEnable(GL_RASTERIZER_DISCARD);
		for (int i = m_IterationsPerFrame; i != 0; --i)
		{
			glBindVertexArray(m_VAO[m_IterationIndex & 1]);
			glBindTexture(GL_TEXTURE_BUFFER, m_TexturePos[m_IterationIndex & 1]);
			m_IterationIndex++;
			glBindBufferBase(GL_TRANSFORM_FEEDBACK_BUFFER, 0, m_VBO[POSITION_A + (m_IterationIndex & 1)]);
			glBindBufferBase(GL_TRANSFORM_FEEDBACK_BUFFER, 1, m_VBO[VELOCITY_A + (m_IterationIndex & 1)]);
			glBeginTransformFeedback(GL_POINTS);
			glDrawArrays(GL_POINTS, 0, points_total);
			glEndTransformFeedback();
		}
		glDisable(GL_RASTERIZER_DISCARD);

		static const GLfloat black[] = { 0.0f, 0.0f, 0.0f, 0.0f };

		glClearBufferfv(GL_COLOR, 0, black);

		m_ShaderRender->Bind();

		if (m_DrawPoints)
		{
			glPointSize(4.0f);
			glDrawArrays(GL_POINTS, 0, points_total);
		}

		if (m_DrawLines)
		{
			glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, m_VIB);
			glDrawElements(GL_LINES, connections_total * 2, GL_UNSIGNED_INT, NULL);
		}
	}

	void SpringMass::OnImGuiRender()
	{
		static char* drawPointsBtnLabel = "Show points";
		static char* drawLinesBtnLabel = "Show lines";

		ImGui::Begin("Properties");
		if (ImGui::Button(drawPointsBtnLabel)) {
			m_DrawPoints = !m_DrawPoints;
			m_DrawPoints ? drawPointsBtnLabel = "Hide points" : drawPointsBtnLabel = "Show points";
		}
		if (ImGui::Button(drawLinesBtnLabel)) {
			m_DrawLines = !m_DrawLines;
			m_DrawLines ? drawLinesBtnLabel = "Hide lines" : drawLinesBtnLabel = "Show lines";
		}
		if (ImGui::Button("Reload shaders")) {
			LoadShader();
		}
		ImGui::SliderInt("Iterations per frame", &m_IterationsPerFrame, 0, 100);
		ImGui::End();
	}

	void SpringMass::LoadShader()
	{
		m_ShaderUpdate = new Shader("assets/shaders/SpringMass_update.glsl");
		static const char* tf_varyings[] =
		{
			"tf_position_mass",
			"tf_velocity"
		};
		m_ShaderUpdate->Bind();
		glTransformFeedbackVaryings(m_ShaderUpdate->GetProgramId(), 2, tf_varyings, GL_SEPARATE_ATTRIBS);

		m_ShaderRender = new Shader("assets/shaders/SpringMass_render.glsl");
	}
}