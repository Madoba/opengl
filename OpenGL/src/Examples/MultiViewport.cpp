#include "MultiViewport.h"

#include <glm/gtc/matrix_transform.hpp>

namespace gl
{
	MultiViewport::MultiViewport()
	{
		m_Shader = new Shader("assets/shaders/MultiViewport.glsl");

        static const uint32_t vertex_indices[] =
        {
            0, 1, 2,
            2, 1, 3,
            2, 3, 4,
            4, 3, 5,
            4, 5, 6,
            6, 5, 7,
            6, 7, 0,
            0, 7, 1,
            6, 0, 2,
            2, 4, 6,
            7, 5, 3,
            7, 3, 1
        };

        static const float vertex_positions[] =
        {
            -0.25f, -0.25f, -0.25f,
            -0.25f,  0.25f, -0.25f,
             0.25f, -0.25f, -0.25f,
             0.25f,  0.25f, -0.25f,
             0.25f, -0.25f,  0.25f,
             0.25f,  0.25f,  0.25f,
            -0.25f, -0.25f,  0.25f,
            -0.25f,  0.25f,  0.25f,
        };

		m_VAO = new VertexArray;
		m_VAO->Bind();

        m_VAB = new VertexArrayBuffer(sizeof(vertex_positions));
        m_VAB->SetData(vertex_positions, sizeof(vertex_positions));
       
        glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, 0, NULL);
        glEnableVertexAttribArray(0);

        m_VIB = new IndexBuffer(sizeof(vertex_indices));
        m_VIB->SetData(vertex_indices, sizeof(vertex_indices));
        m_VUB = new UniformBuffer(4 * sizeof(glm::mat4));
	}

	MultiViewport::~MultiViewport()
	{
        delete m_Shader;
        delete m_VAO;
        delete m_VAB;
        delete m_VIB;
        delete m_VUB;
	}

	void MultiViewport::Draw(double currentTime, uint32_t width, uint32_t height)
	{
        static const GLfloat black[] = { 0.0f, 0.0f, 0.0f, 1.0f };
        static const GLfloat one = 1.0f;

        glClearBufferfv(GL_COLOR, 0, black);
        glClearBufferfv(GL_DEPTH, 0, &one);

        float viewport_width = (float)(7 * width) / 16.0f;
        float viewport_height = (float)(7 * height) / 16.0f;

        glEnable(GL_CULL_FACE);
        // glFrontFace(GL_CW);
        glEnable(GL_DEPTH_TEST);
        glDepthFunc(GL_LEQUAL);

        glViewportIndexedf(0, 0, 0, viewport_width, viewport_height);
        glViewportIndexedf(1,
            width - viewport_width, 0,
            viewport_width, viewport_height);
        glViewportIndexedf(2,
            0, height - viewport_height,
            viewport_width, viewport_height);
        glViewportIndexedf(3,
            width - viewport_width,
            height - viewport_height,
            viewport_width, viewport_height);

        glm::mat4 proj_matrix = glm::perspective(50.0f,
            (float)width / (float)height,
            0.1f,
            1000.0f);

        float f = (float)currentTime * 0.3f;

        glm::mat4* mv_matrix_array = (glm::mat4*)m_VUB->GetMapBuffer(4 * sizeof(glm::mat4));

        for (int i = 0; i < 4; i++)
        {
            mv_matrix_array[i] = proj_matrix *
                glm::translate(glm::mat4(1.0f), glm::vec3(0.0f, 0.0f, -2.0f)) *
                glm::rotate(glm::mat4(1.0f), glm::radians((float)currentTime * 45.0f * (float)(i + 1)), glm::vec3(0.0f, 1.0f, 0.0f)) *
                glm::rotate(glm::mat4(1.0f), glm::radians((float)currentTime * 81.0f * (float)(i + 1)), glm::vec3(1.0f, 0.0f, 0.0f));
        }
        m_VUB->UnMap();
        m_VIB->Bind();
        m_VAO->Bind();
        m_Shader->Bind();
        glDrawElements(GL_TRIANGLES, 36, GL_UNSIGNED_INT, 0);

        glDisable(GL_CULL_FACE);
        glDisable(GL_DEPTH_TEST);
	}
}