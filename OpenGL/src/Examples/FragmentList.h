#pragma once
#include "Shader.h"
#include "Tools/sbmMeshLoader.h"

#include "Events/Event.h"
#include "ImGuiLayer.h"

namespace gl
{
	class FragmentList
	{
	public:
		FragmentList();
		~FragmentList();

		void Draw(double currentTime, float aspect);

		void OnImGuiRender();
		void OnEvent(Event& e);
	private:

		Shader* m_ClearShader;
		Shader* m_AppendShader;
		Shader* m_ResolveShader;

		sbmMesh m_DragonMesh;

		struct uniforms_block
		{
			glm::mat4     mv_matrix;
			glm::mat4     view_matrix;
			glm::mat4     proj_matrix;
		};

		GLuint uniforms_buffer;
		GLuint fragment_buffer;
		GLuint head_pointer_image;
		GLuint atomic_counter_buffer;
		GLuint dummy_vao;

		float m_Scale = 2.5f;
	};
}
