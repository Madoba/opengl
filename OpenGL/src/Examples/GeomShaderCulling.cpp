#include "GeomShaderCulling.h"

#include <glm/gtc/matrix_transform.hpp>

namespace gl
{
	GeomShaderCulling::GeomShaderCulling()
	{
		m_Shader = new Shader("assets/shaders/GeomShaderCulling.glsl");
		m_Mesh.load("assets/meshes/dragon.sbm");
	}

	GeomShaderCulling::~GeomShaderCulling()
	{
		m_Mesh.free();
		delete m_Shader;
	}

	void GeomShaderCulling::Draw(double currentTime, float aspect)
	{
		static const GLfloat black[] = { 0.0f, 0.0f, 0.0f, 1.0f };
		static const GLfloat one = 1.0f;

		glClearBufferfv(GL_COLOR, 0, black);
		glClearBufferfv(GL_DEPTH, 0, &one);

		glEnable(GL_DEPTH_TEST);
		glDepthFunc(GL_LEQUAL);

		glm::mat4 proj_matrix = glm::perspective(51.0f,
			aspect,
			0.1f,
			1000.0f);
		glm::mat4 mv_matrix = glm::translate(glm::mat4(1.0f), glm::vec3(0.0f, 0.0f, -20.0f)) *
			glm::rotate(glm::mat4(1.0f), glm::radians((float)currentTime * 5.0f), glm::vec3(0.0f, 1.0f, 0.0f)) *
			glm::rotate(glm::mat4(1.0f), glm::radians((float)currentTime * 100.0f), glm::vec3(1.0f, 0.0f, 0.0f));

		m_Shader->Bind();
		m_Shader->UploadUniformMat4(proj_matrix * mv_matrix, "mvpMatrix");
		m_Shader->UploadUniformMat4(mv_matrix, "mvMatrix");

		glm::vec3 vViewpoint { sinf(currentTime * 2.1f) * 70.0f, cosf(currentTime * 1.4f) * 70.0f, sinf(currentTime * 0.7f) * 70.0f };
		m_Shader->UploadUniformFloat3(vViewpoint, "viewpoint");

		m_Mesh.render();

		glDisable(GL_DEPTH_TEST);
	}
}