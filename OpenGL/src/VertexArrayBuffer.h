#pragma once

#include "Buffer.h"
#include "VertexArrayBuffer.h"

#include <glad/glad.h>

#include <string>
#include <vector>

namespace gl
{
	
	class VertexArrayBuffer
	{
	public:
		VertexArrayBuffer(const size_t& size);
		~VertexArrayBuffer() = default;

		void Bind() const;
		void Unbind() const;

		void SetData(const void* data, uint32_t size);

		void* GenerateMap(const size_t& size);
		void UnMap();

		const BufferLayout& GetLayout() const { return m_Layout; }
		void SetLayout(const BufferLayout& layout) { m_Layout = layout; }

	private:
		GLuint m_Buffer;
		BufferLayout m_Layout;
		uint32_t m_Offset = 0;
	};
}
