#include "IndirectDrawBuffer.h"

namespace gl
{
	IndirectDrawBuffer::IndirectDrawBuffer(const size_t& size)
	{
		glGenBuffers(1, &m_Buffer);
		glBindBuffer(GL_DRAW_INDIRECT_BUFFER, m_Buffer);
		glBufferData(GL_DRAW_INDIRECT_BUFFER,
			size,
			NULL,
			GL_DYNAMIC_DRAW);
	}

	void IndirectDrawBuffer::Bind() const
	{
		glBindBuffer(GL_DRAW_INDIRECT_BUFFER, m_Buffer);
	}

	void IndirectDrawBuffer::Unbind() const
	{
		glBindBuffer(GL_DRAW_INDIRECT_BUFFER, 0);
	}

	void* IndirectDrawBuffer::GenerateMap(const size_t& size)
	{
		return glMapBufferRange(
			GL_DRAW_INDIRECT_BUFFER,
			0,
			size,
			GL_MAP_WRITE_BIT | GL_MAP_INVALIDATE_BUFFER_BIT);
	}

	void IndirectDrawBuffer::UnMap()
	{
		glUnmapBuffer(GL_DRAW_INDIRECT_BUFFER);
	}
}