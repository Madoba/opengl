#include "VertexArray.h"
#include <glm/glm.hpp>

#include <iostream>

namespace gl
{

	VertexArray::VertexArray()
	{
		glGenVertexArrays(1, &m_VAO);
	}

	VertexArray::VertexArray(uint32_t vao)
	{
		m_VAO = vao;
	}

	VertexArray::~VertexArray()
	{
		glDeleteVertexArrays(1, &m_VAO);
	}

	void VertexArray::Bind()
	{
		glBindVertexArray(m_VAO);
	}

	void VertexArray::Unbind()
	{
		glBindVertexArray(0);
	}

	void VertexArray::AddBuffer(VertexArrayBuffer* vertexBuffer)
	{
		glBindVertexArray(m_VAO);
		vertexBuffer->Bind();
		const auto& layout = vertexBuffer->GetLayout();

		for (const auto& bufferData : layout)
		{
			if (bufferData.DataType == BufferDataType::Float
				|| bufferData.DataType == BufferDataType::Float2
				|| bufferData.DataType == BufferDataType::Float3
				|| bufferData.DataType == BufferDataType::Float4
				|| bufferData.DataType == BufferDataType::Int
				|| bufferData.DataType == BufferDataType::Int2
				|| bufferData.DataType == BufferDataType::Int3
				|| bufferData.DataType == BufferDataType::Int4
				|| bufferData.DataType == BufferDataType::Bool)
			{
				glEnableVertexAttribArray(m_VertexBufferIndex);
				glVertexAttribPointer(m_VertexBufferIndex, 
					bufferData.GetDataCount(), 
					bufferData.GetOpenGLType(), 
					bufferData.Normalized ? GL_TRUE : GL_FALSE, 
					layout.GetStride(),
					(const void*)bufferData.Offset);
				m_VertexBufferIndex++;
				
			}
			else if (bufferData.DataType == BufferDataType::Mat3
				|| bufferData.DataType == BufferDataType::Mat4)
			{
				uint8_t count = bufferData.GetDataCount();
				for (uint8_t i = 0; i < count; i++)
				{
					glEnableVertexAttribArray(m_VertexBufferIndex);
					glVertexAttribPointer(m_VertexBufferIndex,
						count,
						bufferData.GetOpenGLType(),
						bufferData.Normalized ? GL_TRUE : GL_FALSE,
						layout.GetStride(),
						(const void*)(sizeof(float) * count * i));
					glVertexAttribDivisor(m_VertexBufferIndex, 1);
					m_VertexBufferIndex++;
				}
			}
		}
		m_Buffers.push_back(vertexBuffer);
	}

	void VertexArray::AddInstancedBuffer(VertexArrayBuffer* vertexBuffer)
	{
		glBindVertexArray(m_VAO);
		vertexBuffer->Bind();
		const auto& layout = vertexBuffer->GetLayout();
		float offset = 0;

		for (const auto& bufferData : layout)
		{
			if (bufferData.DataType == BufferDataType::Mat4)
			{
				uint8_t count = bufferData.GetDataCount();
				glVertexAttribPointer(m_VertexBufferIndex,
					count,
					bufferData.GetOpenGLType(),
					bufferData.Normalized ? GL_TRUE : GL_FALSE,
					0, 
					(const void*)(size_t)offset);
				offset += sizeof(float) * 16;
				glEnableVertexAttribArray(m_VertexBufferIndex);
				m_VertexBufferIndex++;
			}
		}
		m_Buffers.push_back(vertexBuffer);
	}
}