#include "Window.h"

#include <iostream>

#include "Events/ApplicationEvent.h"
#include "Events/KeyEvent.h"
#include "Events/MouseEvent.h"

namespace gl
{
	Window::Window(const WindowInfo& info)
	{
		m_Info.Title = info.Title;
		m_Info.Width = info.Width;
		m_Info.Height = info.Height;
		m_Info.Flags = info.Flags;
		Init();
	}

	Window::~Window()
	{
		Shutdown();
	}

	void Window::OnUpdate()
	{
		glfwSwapBuffers(m_Window);
		glfwPollEvents();
	}

	void Window::SetEventCallback(const std::function<void(Event&)>& callback)
	{
		m_Info.EventCallback = callback;
	}

	void Window::SetVSync(bool enabled)
	{
		if (enabled)
			glfwSwapInterval(1);
		else
			glfwSwapInterval(0);

		m_Info.Flags.VSync = enabled;
	}

	void Window::Init()
	{
		if (!glfwInit())
		{
			fprintf(stderr, "Failed to initialize GLFW\n");
			return;
		}

		glfwWindowHint(GLFW_CONTEXT_VERSION_MAJOR, 4);
		glfwWindowHint(GLFW_CONTEXT_VERSION_MINOR, 5);

#ifdef GL_DEBUG
		glfwWindowHint(GLFW_OPENGL_DEBUG_CONTEXT, GL_TRUE);
#else
		glfwWindowHint(GLFW_CONTEXT_ROBUSTNESS, GLFW_LOSE_CONTEXT_ON_RESET);
#endif /* _DEBUG */
		glfwWindowHint(GLFW_OPENGL_PROFILE, GLFW_OPENGL_CORE_PROFILE);
		glfwWindowHint(GLFW_OPENGL_FORWARD_COMPAT, GL_TRUE);
		/*glfwWindowHint(GLFW_SAMPLES, m_Info.samples);
		glfwWindowHint(GLFW_STEREO, m_Info.flags.stereo ? GL_TRUE : GL_FALSE);*/

		{
			m_Window = glfwCreateWindow(m_Info.Width, m_Info.Height, m_Info.Title.c_str(), m_Info.Flags.Fullscreen ? glfwGetPrimaryMonitor() : NULL, NULL);
			if (!m_Window)
			{
				fprintf(stderr, "Failed to open window\n");
				return;
			}
		}

		glfwMakeContextCurrent(m_Window);
		SetVSync(true);

		glfwSetWindowUserPointer(m_Window, &m_Info);

		glfwSetWindowSizeCallback(m_Window, [](GLFWwindow* window, int width, int height) 
		{
			WindowInfo& info = *(WindowInfo*)glfwGetWindowUserPointer(window);
			info.Width = width;
			info.Height = height;

			WindowResizeEvent event(width, height);
			info.EventCallback(event);
			//glViewport(0, 0, width, height);
		});

		glfwSetKeyCallback(m_Window, [](GLFWwindow* window, int key, int scancode, int action, int mods) 
		{
			WindowInfo& info = *(WindowInfo*)glfwGetWindowUserPointer(window);
			switch (action)
			{
				case GLFW_PRESS:
				{
					KeyPressedEvent event(static_cast<KeyCode>(key), 0);
					info.EventCallback(event);
					break;
				}
				case GLFW_RELEASE:
				{
					KeyReleasedEvent event(static_cast<KeyCode>(key));
					info.EventCallback(event);
					break;
				}
				case GLFW_REPEAT:
				{
					KeyPressedEvent event(static_cast<KeyCode>(key), 1);
					info.EventCallback(event);
					break;
				}
			}
		});

		glfwSetMouseButtonCallback(m_Window, [](GLFWwindow* window, int button, int action, int mods) 
		{
			WindowInfo& info = *(WindowInfo*)glfwGetWindowUserPointer(window);

			switch (action)
			{
			case GLFW_PRESS:
			{
				MouseButtonPressedEvent event(static_cast<MouseCode>(button));
				info.EventCallback(event);
				break;
			}
			case GLFW_RELEASE:
			{
				MouseButtonReleasedEvent event(static_cast<MouseCode>(button));
				info.EventCallback(event);
				break;
			}
			}
		});

		glfwSetCursorPosCallback(m_Window, [](GLFWwindow* window, double x, double y) 
		{
			WindowInfo& info = *(WindowInfo*)glfwGetWindowUserPointer(window);

			MouseMovedEvent event((float)x, (float)y);
			info.EventCallback(event);
		});

		glfwSetScrollCallback(m_Window, [](GLFWwindow* window, double xoffset, double yoffset) 
		{
			WindowInfo& info = *(WindowInfo*)glfwGetWindowUserPointer(window);

			MouseScrolledEvent event((float)xoffset, (float)yoffset);
			info.EventCallback(event);
		});

		glfwSetWindowCloseCallback(m_Window, [](GLFWwindow* window)
		{
			WindowInfo& info = *(WindowInfo*)glfwGetWindowUserPointer(window);
			WindowCloseEvent event;
			info.EventCallback(event);
		});

		gladLoadGLLoader((GLADloadproc)glfwGetProcAddress);

	}
	void Window::Shutdown()
	{
		glfwDestroyWindow(m_Window);
		glfwTerminate();
	}
}