#pragma once
/* Credits */
/* From OpenGL Superbible source code 7thj edition*/

#include <cstdint>

namespace gl {

    class KTXLoader
    {
    public:
        struct header
        {
            unsigned char       identifier[12];
            unsigned int        endianness;
            unsigned int        gltype;
            unsigned int        gltypesize;
            unsigned int        glformat;
            unsigned int        glinternalformat;
            unsigned int        glbaseinternalformat;
            unsigned int        pixelwidth;
            unsigned int        pixelheight;
            unsigned int        pixeldepth;
            unsigned int        arrayelements;
            unsigned int        faces;
            unsigned int        miplevels;
            unsigned int        keypairbytes;
        };

        union keyvaluepair
        {
            unsigned int        size;
            unsigned char       rawbytes[4];
        };

    public:
        KTXLoader(const char* filename);
        ~KTXLoader() = default;

        uint32_t load(uint32_t tex = 0);
        bool save(uint32_t target, uint32_t tex);

    private:
        const uint32_t swap32(const uint32_t u32);
        const uint32_t swap16(const uint32_t u16);
        uint32_t calculate_stride(const header& h, uint32_t width, uint32_t pad = 4);
        uint32_t calculate_face_size(const header& h);
    private:
        const char* m_Filename;
        const unsigned char m_Identifier[12] = { 0xAB, 0x4B, 0x54, 0x58, 0x20, 0x31, 0x31, 0xBB, 0x0D,0x0A, 0x1A, 0x0A };
    };
}
