#include "Texture2DArray.h"

#include <glad/glad.h>
#include "Tools/KTXLoader.h"

namespace gl
{
	Texture2DArray::Texture2DArray(const char* filePath)
	{
		glCreateTextures(GL_TEXTURE_2D_ARRAY, 1, &m_TextureID);
		KTXLoader loader(filePath);
		loader.load(m_TextureID);
		glBindTexture(GL_TEXTURE_2D_ARRAY, m_TextureID);
		glTexParameteri(GL_TEXTURE_2D_ARRAY, GL_TEXTURE_MIN_FILTER, GL_LINEAR_MIPMAP_LINEAR);
	}

	Texture2DArray::~Texture2DArray()
	{
		glDeleteTextures(1, &m_TextureID);
	}

	void Texture2DArray::Bind() const
	{
		glBindTexture(GL_TEXTURE_2D_ARRAY, m_TextureID);
	}

	void Texture2DArray::Unbind() const
	{
		glBindTexture(GL_TEXTURE_2D_ARRAY, 0);
	}
}