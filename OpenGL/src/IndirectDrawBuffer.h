#pragma once

#include <glad/glad.h>

namespace gl
{
	class IndirectDrawBuffer
	{
	public:
		IndirectDrawBuffer(const size_t& size);
		~IndirectDrawBuffer() = default;

		void Bind() const;
		void Unbind() const;
		
		void* GenerateMap(const size_t& size);
		void UnMap();

	private:
		GLuint m_Buffer;
	};
}