#pragma once
#include <vector>

#include "VertexArrayBuffer.h"

#include <glad/glad.h>

namespace gl
{
	class VertexArray
	{
	public:
		VertexArray();
		VertexArray(uint32_t vao);
		~VertexArray();

		void Bind();
		void Unbind();

		void AddBuffer(VertexArrayBuffer* vertexBuffer);
		void AddInstancedBuffer(VertexArrayBuffer* vertexBuffer);

	private:
		uint32_t m_VAO;
		uint32_t m_VertexBufferIndex = 0;
		std::vector<VertexArrayBuffer*> m_Buffers;
	};
}
