#include "VertexArrayBuffer.h"

#include <iostream>

namespace gl
{
	VertexArrayBuffer::VertexArrayBuffer(const size_t& size)
	{
		glGenBuffers(1, &m_Buffer);
		glBindBuffer(GL_ARRAY_BUFFER, m_Buffer);
		glBufferData(GL_ARRAY_BUFFER, size, NULL, GL_DYNAMIC_DRAW);
	}

	void VertexArrayBuffer::Bind() const
	{
		glBindBuffer(GL_ARRAY_BUFFER, m_Buffer);
	}

	void VertexArrayBuffer::Unbind() const
	{
		glBindBuffer(GL_ARRAY_BUFFER, 0);
	}

	void VertexArrayBuffer::SetData(const void* data, uint32_t size)
	{
		glBindBuffer(GL_ARRAY_BUFFER, m_Buffer);
		glBufferSubData(GL_ARRAY_BUFFER, m_Offset, size, data);
		m_Offset += size;
	}

	void* VertexArrayBuffer::GenerateMap(const size_t& size)
	{
		glBindBuffer(GL_ARRAY_BUFFER, m_Buffer);
		return glMapBufferRange(
			GL_ARRAY_BUFFER,
			0,
			size,
			GL_MAP_WRITE_BIT |
			GL_MAP_INVALIDATE_BUFFER_BIT);
	}

	void VertexArrayBuffer::UnMap()
	{
		glUnmapBuffer(GL_ARRAY_BUFFER);
	}
}