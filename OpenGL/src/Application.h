#pragma once

#include <glad/glad.h>
#include <GLFW/glfw3.h>

#define GLFW_NO_GLU 1
#define GLFW_INCLUDE_GLCOREARB 1

#include "Window.h"
#include "ImGuiLayer.h"

#include "Examples/FilledTriangle.h"
#include "Examples/TessellatedTriangle.h"
#include "Examples/SpinningCube.h"
#include "Examples/SimpleTexture.h"
#include "Examples/TextureKTX.h"
#include "Examples/WrapTexture.h"
#include "Examples/Tunnel.h"
#include "Examples/WrapModes.h"
#include "Examples/MirrorClampEdge.h"
#include "Examples/AlienRain.h"
#include "Examples/FragmentList.h"
#include "Examples/IndexedCube.h"
#include "Examples/Grass.h"
#include "Examples/InstancedAttributes.h"
#include "Examples/MultiDrawIndirect.h"
#include "Examples/SpringMass.h"
#include "Examples/ClipDistance.h"
#include "Examples/TessModes.h"
#include "Examples/DisplacementMap.h"
#include "Examples/CubicBezier.h"
#include "Examples/GeomShaderCulling.h"
#include "Examples/ObjectExploder.h"
#include "Examples/GeoShaderTessellate.h"
#include "Examples/NormalViewer.h"
#include "Examples/GeoShaderQuad.h"
#include "Examples/MultiViewport.h"
#include "Examples/MultiScissor.h"

namespace gl
{

	class Application
	{
	public:
		struct AppInfo
		{
			char title[128];
			int windowWidth;
			int windowHeight;
			int majorVersion;
			int minorVersion;
			int samples;
			union
			{
				struct
				{
					unsigned int    fullscreen : 1;
					unsigned int    vsync : 1;
					unsigned int    cursor : 1;
					unsigned int    stereo : 1;
					unsigned int    debug : 1;
					unsigned int    robust : 1;
				};
				unsigned int        all;
			} flags;
		};

	public:
		Application() = default;
		virtual ~Application() = default;

		void Run();

		void startup();
		void render(double currentTime);
		void OnImGuiRender();
		void shutdown();

		void OnEvent(Event& e);

		Window& GetWindow() { return *m_Window; }
		static Application& Get() { return *s_Instance; }
	private:
		void setVsync(bool enable);

	private:
		bool m_Running = true;
		AppInfo m_Info;

		Window* m_Window;
		ImGuiLayer* m_ImGui;

		std::vector<ImGuiLayer::ExamplesCheckBoxes> m_ExamplesStates;

		FilledTriangle* m_FilledTriangle;
		TessellatedTriangle* m_TesselatedTriangle;
		SpinningCube* m_SpinningCube;
		SimpleTexture* m_SimpleTexture;
		TextureKTX* m_TextureKTX;
		WrapTexture* m_WrapTexture;
		Tunnel* m_Tunnel;
		WrapModes* m_WrapModes;
		MirrorClampEdge* m_MirrorClampEdge;
		AlienRain* m_AlienRain;
		FragmentList* m_FragmentList;
		IndexedCube* m_IndexedCube;
		Grass* m_Grass;
		InstancedAttributes* m_InstancedAttributes;
		MultiDrawIndirect* m_MultiDrawIndirect;
		SpringMass* m_SpringMass;
		ClipDistance* m_ClipDistance;
		TessModes* m_TessModes;
		DisplacementMap* m_DisplacementMap;
		CubicBezier* m_CubicBezier;
		GeomShaderCulling* m_GeomShaderCulling;
		ObjectExploder* m_ObjectExploder;
		GeoShaderTessellate* m_GeoShaderTessellate;
		NormalViewer* m_NormalViewer;
		GeoShaderQuad* m_GeoShaderQuad;
		MultiViewport* m_MultiViewport;
		MultiScissor* m_MultiScissor;

	private:
		static Application* s_Instance;
	};
}