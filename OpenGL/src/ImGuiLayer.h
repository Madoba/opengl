#pragma once

#include "Events/Event.h"

#include <vector>
#include <string>

namespace gl
{

	class ImGuiLayer
	{
	public:
		struct ExamplesCheckBoxes
		{
			std::string name;
			bool state;
		};
	public:
		ImGuiLayer() = default;
		~ImGuiLayer() = default;

		void StartUp();
		void OnRender(std::vector<ExamplesCheckBoxes>& examplesStates);
		void OnEvent(Event& e);
		void OnShutDown();

		void Begin();
		void End();
	};

}