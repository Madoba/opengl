#include "Texture2D.h"

#include <glad/glad.h>
#include "Tools/KTXLoader.h"

namespace gl {

	Texture2D::Texture2D(const char* filePath)
	{
		// Only works for .KTX files
		// TODO: abstract to more formats
		glCreateTextures(GL_TEXTURE_2D, 1, &m_TextureID);
		KTXLoader loader(filePath);
		loader.load(m_TextureID);
	}

	Texture2D::~Texture2D()
	{
		glDeleteTextures(1, &m_TextureID);
	}

	void Texture2D::Bind() const
	{
		glBindTexture(GL_TEXTURE_2D, m_TextureID);
	}

	void Texture2D::Unbind() const
	{
		glBindTexture(GL_TEXTURE_2D, 0);
	}

	void Texture2D::SetFilterToMipmapLinear()
	{
		glTextureParameteri(m_TextureID, GL_TEXTURE_WRAP_S, GL_REPEAT);
		glTextureParameteri(m_TextureID, GL_TEXTURE_WRAP_T, GL_REPEAT);

		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR_MIPMAP_LINEAR);
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
	}

}