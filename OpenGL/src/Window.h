#pragma once
#include <string>
#include <functional>

#include <glad/glad.h>
#include <GLFW/glfw3.h>

#include "Events/Event.h"


namespace gl
{
	struct WindowInfo
	{
		std::string Title;
		uint32_t Width, Height;

		union
		{
			struct
			{
				unsigned int Fullscreen : 1;
				unsigned int VSync : 1;
			};
			unsigned int all;
		} Flags;

		std::function<void(Event&)> EventCallback;
	};


	class Window
	{
	public:
		Window(const WindowInfo& info);
		~Window();

		void OnUpdate();

		uint32_t GetWidth() const { return m_Info.Width; }
		uint32_t GetHeight() const { return m_Info.Height; }

		GLFWwindow* GetNative() const { return m_Window; }

		void SetEventCallback(const std::function<void(Event&)>& callback);
		void SetVSync(bool enabled);
		bool isVSync() { return m_Info.Flags.VSync; }

	private:
		void Init();
		void Shutdown();

	private:
		GLFWwindow* m_Window;
		WindowInfo m_Info;
	};

}
