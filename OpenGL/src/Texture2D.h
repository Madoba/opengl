#pragma once
#include <cstdint>

namespace gl
{
	class Texture2D
	{
	public:
		Texture2D() = default;
		Texture2D(const char* filePath);
		~Texture2D();

		void Bind() const;
		void Unbind() const;

		void SetFilterToMipmapLinear();

	private:
		uint32_t m_TextureID;
	};

}