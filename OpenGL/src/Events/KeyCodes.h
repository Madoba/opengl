#pragma once
#include <cstdint>
#include <ostream>

namespace gl
{
	typedef enum class KeyCode : uint16_t
	{
		// From glfw3.h
		Space               = 32,
		Apostrophe          = 39, /* ' */
		Comma               = 44, /* , */
		Minus               = 45, /* - */
		Period              = 46, /* . */
		Slash               = 47, /* / */

		D0                  = 48, /* 0 */
		D1                  = 49, /* 1 */
		D2                  = 50, /* 2 */
		D3                  = 51, /* 3 */
		D4                  = 52, /* 4 */
		D5                  = 53, /* 5 */
		D6                  = 54, /* 6 */
		D7                  = 55, /* 7 */
		D8                  = 56, /* 8 */
		D9                  = 57, /* 9 */

		Semicolon           = 59, /* ; */
		Equal               = 61, /* = */

		A                   = 65,
		B                   = 66,
		C                   = 67,
		D                   = 68,
		E                   = 69,
		F                   = 70,
		G                   = 71,
		H                   = 72,
		I                   = 73,
		J                   = 74,
		K                   = 75,
		L                   = 76,
		M                   = 77,
		N                   = 78,
		O                   = 79,
		P                   = 80,
		Q                   = 81,
		R                   = 82,
		S                   = 83,
		T                   = 84,
		U                   = 85,
		V                   = 86,
		W                   = 87,
		X                   = 88,
		Y                   = 89,
		Z                   = 90,

		LeftBracket         = 91,  /* [ */
		Backslash           = 92,  /* \ */
		RightBracket        = 93,  /* ] */
		GraveAccent         = 96,  /* ` */

		World1              = 161, /* non-US #1 */
		World2              = 162, /* non-US #2 */

		/* Function keys */
		Escape              = 256,
		Enter               = 257,
		Tab                 = 258,
		Backspace           = 259,
		Insert              = 260,
		Delete              = 261,
		Right               = 262,
		Left                = 263,
		Down                = 264,
		Up                  = 265,
		PageUp              = 266,
		PageDown            = 267,
		Home                = 268,
		End                 = 269,
		CapsLock            = 280,
		ScrollLock          = 281,
		NumLock             = 282,
		PrintScreen         = 283,
		Pause               = 284,
		F1                  = 290,
		F2                  = 291,
		F3                  = 292,
		F4                  = 293,
		F5                  = 294,
		F6                  = 295,
		F7                  = 296,
		F8                  = 297,
		F9                  = 298,
		F10                 = 299,
		F11                 = 300,
		F12                 = 301,
		F13                 = 302,
		F14                 = 303,
		F15                 = 304,
		F16                 = 305,
		F17                 = 306,
		F18                 = 307,
		F19                 = 308,
		F20                 = 309,
		F21                 = 310,
		F22                 = 311,
		F23                 = 312,
		F24                 = 313,
		F25                 = 314,

		/* Keypad */
		KP0                 = 320,
		KP1                 = 321,
		KP2                 = 322,
		KP3                 = 323,
		KP4                 = 324,
		KP5                 = 325,
		KP6                 = 326,
		KP7                 = 327,
		KP8                 = 328,
		KP9                 = 329,
		KPDecimal           = 330,
		KPDivide            = 331,
		KPMultiply          = 332,
		KPSubtract          = 333,
		KPAdd               = 334,
		KPEnter             = 335,
		KPEqual             = 336,

		LeftShift           = 340,
		LeftControl         = 341,
		LeftAlt             = 342,
		LeftSuper           = 343,
		RightShift          = 344,
		RightControl        = 345,
		RightAlt            = 346,
		RightSuper          = 347,
		Menu                = 348
	} Key;

	inline std::ostream& operator<<(std::ostream& os, KeyCode keyCode)
	{
		os << static_cast<int32_t>(keyCode);
		return os;
	}
}

// From glfw3.h
#define GL_KEY_SPACE           ::gl::Key::Space
#define GL_KEY_APOSTROPHE      ::gl::Key::Apostrophe    /* ' */
#define GL_KEY_COMMA           ::gl::Key::Comma         /* , */
#define GL_KEY_MINUS           ::gl::Key::Minus         /* - */
#define GL_KEY_PERIOD          ::gl::Key::Period        /* . */
#define GL_KEY_SLASH           ::gl::Key::Slash         /* / */
#define GL_KEY_0               ::gl::Key::D0
#define GL_KEY_1               ::gl::Key::D1
#define GL_KEY_2               ::gl::Key::D2
#define GL_KEY_3               ::gl::Key::D3
#define GL_KEY_4               ::gl::Key::D4
#define GL_KEY_5               ::gl::Key::D5
#define GL_KEY_6               ::gl::Key::D6
#define GL_KEY_7               ::gl::Key::D7
#define GL_KEY_8               ::gl::Key::D8
#define GL_KEY_9               ::gl::Key::D9
#define GL_KEY_SEMICOLON       ::gl::Key::Semicolon     /* ; */
#define GL_KEY_EQUAL           ::gl::Key::Equal         /* = */
#define GL_KEY_A               ::gl::Key::A
#define GL_KEY_B               ::gl::Key::B
#define GL_KEY_C               ::gl::Key::C
#define GL_KEY_D               ::gl::Key::D
#define GL_KEY_E               ::gl::Key::E
#define GL_KEY_F               ::gl::Key::F
#define GL_KEY_G               ::gl::Key::G
#define GL_KEY_H               ::gl::Key::H
#define GL_KEY_I               ::gl::Key::I
#define GL_KEY_J               ::gl::Key::J
#define GL_KEY_K               ::gl::Key::K
#define GL_KEY_L               ::gl::Key::L
#define GL_KEY_M               ::gl::Key::M
#define GL_KEY_N               ::gl::Key::N
#define GL_KEY_O               ::gl::Key::O
#define GL_KEY_P               ::gl::Key::P
#define GL_KEY_Q               ::gl::Key::Q
#define GL_KEY_R               ::gl::Key::R
#define GL_KEY_S               ::gl::Key::S
#define GL_KEY_T               ::gl::Key::T
#define GL_KEY_U               ::gl::Key::U
#define GL_KEY_V               ::gl::Key::V
#define GL_KEY_W               ::gl::Key::W
#define GL_KEY_X               ::gl::Key::X
#define GL_KEY_Y               ::gl::Key::Y
#define GL_KEY_Z               ::gl::Key::Z
#define GL_KEY_LEFT_BRACKET    ::gl::Key::LeftBracket   /* [ */
#define GL_KEY_BACKSLASH       ::gl::Key::Backslash     /* \ */
#define GL_KEY_RIGHT_BRACKET   ::gl::Key::RightBracket  /* ] */
#define GL_KEY_GRAVE_ACCENT    ::gl::Key::GraveAccent   /* ` */
#define GL_KEY_WORLD_1         ::gl::Key::World1        /* non-US #1 */
#define GL_KEY_WORLD_2         ::gl::Key::World2        /* non-US #2 */

/* Function keys */
#define GL_KEY_ESCAPE          ::gl::Key::Escape
#define GL_KEY_ENTER           ::gl::Key::Enter
#define GL_KEY_TAB             ::gl::Key::Tab
#define GL_KEY_BACKSPACE       ::gl::Key::Backspace
#define GL_KEY_INSERT          ::gl::Key::Insert
#define GL_KEY_DELETE          ::gl::Key::Delete
#define GL_KEY_RIGHT           ::gl::Key::Right
#define GL_KEY_LEFT            ::gl::Key::Left
#define GL_KEY_DOWN            ::gl::Key::Down
#define GL_KEY_UP              ::gl::Key::Up
#define GL_KEY_PAGE_UP         ::gl::Key::PageUp
#define GL_KEY_PAGE_DOWN       ::gl::Key::PageDown
#define GL_KEY_HOME            ::gl::Key::Home
#define GL_KEY_END             ::gl::Key::End
#define GL_KEY_CAPS_LOCK       ::gl::Key::CapsLock
#define GL_KEY_SCROLL_LOCK     ::gl::Key::ScrollLock
#define GL_KEY_NUM_LOCK        ::gl::Key::NumLock
#define GL_KEY_PRINT_SCREEN    ::gl::Key::PrintScreen
#define GL_KEY_PAUSE           ::gl::Key::Pause
#define GL_KEY_F1              ::gl::Key::F1
#define GL_KEY_F2              ::gl::Key::F2
#define GL_KEY_F3              ::gl::Key::F3
#define GL_KEY_F4              ::gl::Key::F4
#define GL_KEY_F5              ::gl::Key::F5
#define GL_KEY_F6              ::gl::Key::F6
#define GL_KEY_F7              ::gl::Key::F7
#define GL_KEY_F8              ::gl::Key::F8
#define GL_KEY_F9              ::gl::Key::F9
#define GL_KEY_F10             ::gl::Key::F10
#define GL_KEY_F11             ::gl::Key::F11
#define GL_KEY_F12             ::gl::Key::F12
#define GL_KEY_F13             ::gl::Key::F13
#define GL_KEY_F14             ::gl::Key::F14
#define GL_KEY_F15             ::gl::Key::F15
#define GL_KEY_F16             ::gl::Key::F16
#define GL_KEY_F17             ::gl::Key::F17
#define GL_KEY_F18             ::gl::Key::F18
#define GL_KEY_F19             ::gl::Key::F19
#define GL_KEY_F20             ::gl::Key::F20
#define GL_KEY_F21             ::gl::Key::F21
#define GL_KEY_F22             ::gl::Key::F22
#define GL_KEY_F23             ::gl::Key::F23
#define GL_KEY_F24             ::gl::Key::F24
#define GL_KEY_F25             ::gl::Key::F25

/* Keypad */
#define GL_KEY_KP_0            ::gl::Key::KP0
#define GL_KEY_KP_1            ::gl::Key::KP1
#define GL_KEY_KP_2            ::gl::Key::KP2
#define GL_KEY_KP_3            ::gl::Key::KP3
#define GL_KEY_KP_4            ::gl::Key::KP4
#define GL_KEY_KP_5            ::gl::Key::KP5
#define GL_KEY_KP_6            ::gl::Key::KP6
#define GL_KEY_KP_7            ::gl::Key::KP7
#define GL_KEY_KP_8            ::gl::Key::KP8
#define GL_KEY_KP_9            ::gl::Key::KP9
#define GL_KEY_KP_DECIMAL      ::gl::Key::KPDecimal
#define GL_KEY_KP_DIVIDE       ::gl::Key::KPDivide
#define GL_KEY_KP_MULTIPLY     ::gl::Key::KPMultiply
#define GL_KEY_KP_SUBTRACT     ::gl::Key::KPSubtract
#define GL_KEY_KP_ADD          ::gl::Key::KPAdd
#define GL_KEY_KP_ENTER        ::gl::Key::KPEnter
#define GL_KEY_KP_EQUAL        ::gl::Key::KPEqual

#define GL_KEY_LEFT_SHIFT      ::gl::Key::LeftShift
#define GL_KEY_LEFT_CONTROL    ::gl::Key::LeftControl
#define GL_KEY_LEFT_ALT        ::gl::Key::LeftAlt
#define GL_KEY_LEFT_SUPER      ::gl::Key::LeftSuper
#define GL_KEY_RIGHT_SHIFT     ::gl::Key::RightShift
#define GL_KEY_RIGHT_CONTROL   ::gl::Key::RightControl
#define GL_KEY_RIGHT_ALT       ::gl::Key::RightAlt
#define GL_KEY_RIGHT_SUPER     ::gl::Key::RightSuper
#define GL_KEY_MENU            ::gl::Key::Menu
