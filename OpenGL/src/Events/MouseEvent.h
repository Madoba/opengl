#pragma once
#include "Event.h"
#include "MouseCodes.h"

namespace gl
{
	class MouseMovedEvent : public Event
	{
	public:
		MouseMovedEvent(float x, float y)
			: m_MouseX(x), m_MouseY(y) {}

		float GetX() const { return m_MouseX; }
		float GetY() const { return m_MouseY; }

		virtual EventType GetEventType() const override { return EventType::MouseMoved; }
		virtual const char* GetName() const override { return "MouseMoved"; }

		static EventType GetStaticType() { return EventType::MouseMoved; }
	private:
		float m_MouseX, m_MouseY;
	};

	class MouseScrolledEvent : public Event
	{
	public:
		MouseScrolledEvent(float xOffset, float yOffset)
			: m_XOffset(xOffset), m_YOffset(yOffset) {}

		float GetXOffset() const { return m_XOffset; }
		float GetYOffset() const { return m_YOffset; }

		virtual EventType GetEventType() const override { return EventType::MouseScrolled; }
		virtual const char* GetName() const override { return "MouseScrolled"; }

		static EventType GetStaticType() { return EventType::MouseScrolled; }
	private:
		float m_XOffset, m_YOffset;
	};

	class MouseButtonEvent : public Event
	{
	public:
		inline MouseCode GetMouseButton() const { return m_Button; }

	protected:
		MouseButtonEvent(MouseCode button)
			: m_Button(button) {}

		MouseCode m_Button;
	};

	class MouseButtonPressedEvent : public MouseButtonEvent
	{
	public:
		MouseButtonPressedEvent(MouseCode button)
			: MouseButtonEvent(button) {}

		virtual EventType GetEventType() const override { return EventType::MouseButtonPressed; }
		virtual const char* GetName() const override { return "MouseButtonPressed"; }

		static EventType GetStaticType() { return EventType::MouseButtonPressed; }
	};


	class MouseButtonReleasedEvent : public MouseButtonEvent
	{
	public:
		MouseButtonReleasedEvent(MouseCode button)
			: MouseButtonEvent(button) {}

		virtual EventType GetEventType() const override { return EventType::MouseButtonReleased; }
		virtual const char* GetName() const override { return "MouseButtonReleased"; }

		static EventType GetStaticType() { return EventType::MouseButtonReleased; }
	};

}
