#pragma once
#include <cstdint>

#include "Event.h"

namespace gl
{
	class WindowResizeEvent : public Event
	{
	public:
		WindowResizeEvent(uint32_t width, uint32_t height)
			: Width(width), Height(height) {}

		uint32_t GetWidth() const { return Width; }
		uint32_t GetHeight() const { return Height; }

		virtual EventType GetEventType() const override { return EventType::WindowResize; }
		virtual const char* GetName() const override { return "WindowResize"; }

		static EventType GetStaticType() { return EventType::WindowResize; }
	private:
		uint32_t Width;
		uint32_t Height;
	};

	class WindowCloseEvent : public Event
	{
	public:
		WindowCloseEvent() = default;

		virtual EventType GetEventType() const override { return EventType::WindowClose; }
		virtual const char* GetName() const override { return "WindowClose"; }

		static EventType GetStaticType() { return EventType::WindowClose; }
	};
}
