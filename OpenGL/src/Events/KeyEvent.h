#pragma once
#include "Event.h"
#include "KeyCodes.h"

namespace gl
{
	class KeyEvent : public Event
	{
	public:
		KeyCode GetKeyCode() const { return m_KeyCode; }

	protected:
		KeyEvent(KeyCode keycode)
			: m_KeyCode(keycode) {}

		KeyCode m_KeyCode;
	};

	class KeyPressedEvent : public KeyEvent
	{
	public:
		KeyPressedEvent(KeyCode keycode, int repeatCount)
			:KeyEvent(keycode), m_RepeatCount(repeatCount) {}

		int GetRepeatCount() const { return m_RepeatCount; }

		virtual EventType GetEventType() const override { return EventType::KeyPressed; }
		virtual const char* GetName() const { return "KeyPressed"; }

		static EventType GetStaticType() { return EventType::KeyPressed; }
	private:
		int m_RepeatCount;
	};

	class KeyReleasedEvent : public KeyEvent
	{
	public:
		KeyReleasedEvent(KeyCode keycode)
			: KeyEvent(keycode) {}

		virtual EventType GetEventType() const override { return EventType::KeyReleased; }
		virtual const char* GetName() const { return "KeyReleased"; }

		static EventType GetStaticType() { return EventType::KeyReleased; }
	};

	class KeyTypedEvent : public KeyEvent
	{
	public:
		KeyTypedEvent(KeyCode keycode)
			: KeyEvent(keycode) {}

		virtual EventType GetEventType() const override { return EventType::KeyTyped; }
		virtual const char* GetName() const { return "KeyTyped"; }

		static EventType GetStaticType() { return EventType::KeyTyped; }
	};
}
