#pragma once

namespace gl
{
	enum class EventType
	{
		None = 0,
		WindowClose, WindowResize, 
		KeyPressed, KeyReleased, KeyTyped, 
		MouseButtonPressed, MouseButtonReleased, MouseMoved, MouseScrolled
	};

	class Event
	{
	public:
		bool Handled = false;
	public:
		virtual ~Event() = default;

		virtual EventType GetEventType() const = 0;
		virtual const char* GetName() const = 0;
	};

	class EventDispatcher
	{
	public:
		EventDispatcher(Event& event)
			: m_Event(event) {}

		template<typename T, typename F>
		bool Dispatch(const F& func)
		{
			if (m_Event.GetEventType() == T::GetStaticType())
			{
				m_Event.Handled = func(static_cast<T&>(m_Event));
			}
			return false;
		}

	private:
		Event& m_Event;
	};
}
