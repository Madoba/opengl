#pragma once
#include <cstdint>
#include <ostream>

namespace gl
{
	typedef enum class MouseCode : uint16_t
	{
		// From glfw3.h
		Button0                = 0,
		Button1                = 1,
		Button2                = 2,
		Button3                = 3,
		Button4                = 4,
		Button5                = 5,
		Button6                = 6,
		Button7                = 7,

		ButtonLast             = Button7,
		ButtonLeft             = Button0,
		ButtonRight            = Button1,
		ButtonMiddle           = Button2
	} Mouse;
	
	inline std::ostream& operator<<(std::ostream& os, MouseCode mouseCode)
	{
		os << static_cast<int32_t>(mouseCode);
		return os;
	}
}

#define GL_MOUSE_BUTTON_0      ::gl::Mouse::Button0
#define GL_MOUSE_BUTTON_1      ::gl::Mouse::Button1
#define GL_MOUSE_BUTTON_2      ::gl::Mouse::Button2
#define GL_MOUSE_BUTTON_3      ::gl::Mouse::Button3
#define GL_MOUSE_BUTTON_4      ::gl::Mouse::Button4
#define GL_MOUSE_BUTTON_5      ::gl::Mouse::Button5
#define GL_MOUSE_BUTTON_6      ::gl::Mouse::Button6
#define GL_MOUSE_BUTTON_7      ::gl::Mouse::Button7
#define GL_MOUSE_BUTTON_LAST   ::gl::Mouse::ButtonLast
#define GL_MOUSE_BUTTON_LEFT   ::gl::Mouse::ButtonLeft
#define GL_MOUSE_BUTTON_RIGHT  ::gl::Mouse::ButtonRight
#define GL_MOUSE_BUTTON_MIDDLE ::gl::Mouse::ButtonMiddle
