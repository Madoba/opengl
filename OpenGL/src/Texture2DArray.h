#pragma once
#include <cstdint>

namespace gl
{
	class Texture2DArray
	{
	public:
		Texture2DArray(const char* filePath);
		~Texture2DArray();

		void Bind() const;
		void Unbind() const;

	private:
		uint32_t m_TextureID;
	};

}
