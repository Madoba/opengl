#pragma once
#include <glad/glad.h>

#include <string>
#include <vector>

namespace gl
{
	enum struct BufferDataType
	{
		None = 0, Float, Float2, Float3, Float4, Mat3, Mat4, Int, Int2, Int3, Int4, Bool
	};

	static uint32_t BufferDataTypeSize(BufferDataType type)
	{
		switch (type)
		{
		case BufferDataType::Float:    return 4;
		case BufferDataType::Float2:   return 4 * 2;
		case BufferDataType::Float3:   return 4 * 3;
		case BufferDataType::Float4:   return 4 * 4;
		case BufferDataType::Mat3:     return 4 * 3 * 3;
		case BufferDataType::Mat4:     return 4 * 4 * 4;
		case BufferDataType::Int:      return 4;
		case BufferDataType::Int2:     return 4 * 2;
		case BufferDataType::Int3:     return 4 * 3;
		case BufferDataType::Int4:     return 4 * 4;
		case BufferDataType::Bool:     return 1;
		}
		return 0;
	}

	struct BufferData
	{
		std::string Name;
		BufferDataType DataType;
		uint32_t Size;
		size_t Offset;
		bool Normalized;

		BufferData() = default;

		BufferData(BufferDataType type, const std::string& name, bool normalized = false)
			: Name(name), DataType(type), Size(BufferDataTypeSize(type)), Offset(0), Normalized(normalized)
		{}

		GLenum GetOpenGLType() const
		{
			switch (DataType)
			{
			case BufferDataType::Float:    return GL_FLOAT;
			case BufferDataType::Float2:   return GL_FLOAT;
			case BufferDataType::Float3:   return GL_FLOAT;
			case BufferDataType::Float4:   return GL_FLOAT;
			case BufferDataType::Mat3:     return GL_FLOAT;
			case BufferDataType::Mat4:     return GL_FLOAT;
			case BufferDataType::Int:      return GL_INT;
			case BufferDataType::Int2:     return GL_INT;
			case BufferDataType::Int3:     return GL_INT;
			case BufferDataType::Int4:     return GL_INT;
			case BufferDataType::Bool:     return GL_BOOL;
			};
			return 0;
		}

		uint32_t GetDataCount() const
		{
			switch (DataType)
			{
			case BufferDataType::Float:   return 1;
			case BufferDataType::Float2:  return 2;
			case BufferDataType::Float3:  return 3;
			case BufferDataType::Float4:  return 4;
			case BufferDataType::Mat3:    return 3; // 3* float3
			case BufferDataType::Mat4:    return 4; // 4* float4
			case BufferDataType::Int:     return 1;
			case BufferDataType::Int2:    return 2;
			case BufferDataType::Int3:    return 3;
			case BufferDataType::Int4:    return 4;
			case BufferDataType::Bool:    return 1;
			}
			return 0;
		}
	};

	class BufferLayout
	{
	public:
		BufferLayout() = default;

		BufferLayout(const std::initializer_list<BufferData>& elements)
			: m_Elements(elements)
		{
			CalculateOffsetsAndStride();
		}

		uint32_t GetStride() const { return m_Stride; }
		const std::vector<BufferData>& GetElements() const { return m_Elements; }

		std::vector<BufferData>::iterator begin() { return m_Elements.begin(); }
		std::vector<BufferData>::iterator end() { return m_Elements.end(); }
		std::vector<BufferData>::const_iterator begin() const { return m_Elements.begin(); }
		std::vector<BufferData>::const_iterator end() const { return m_Elements.end(); }
	private:
		void CalculateOffsetsAndStride()
		{
			size_t offset = 0;
			m_Stride = 0;
			for (auto& element : m_Elements)
			{
				element.Offset = offset;
				offset += element.Size;
				m_Stride += element.Size;
			}
		}
	private:
		std::vector<BufferData> m_Elements;
		uint32_t m_Stride = 0;
	};

}
