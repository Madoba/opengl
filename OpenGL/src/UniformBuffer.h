#pragma once
#include "Buffer.h"

#include <glad/glad.h>

#include <string>
#include <vector>

namespace gl
{

	class UniformBuffer
	{
	public:
		UniformBuffer(const size_t& size);
		~UniformBuffer() = default;

		void Bind() const;
		void Unbind() const;

		void* GetMapBuffer(const size_t& size);
		void UnMap();
	private:
		GLuint m_Buffer;
	};
}
