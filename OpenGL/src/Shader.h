#pragma once

#include <glad/glad.h>

#include <glm/glm.hpp>

#include <string>
#include <unordered_map>

namespace gl
{
	class Shader
	{
	public:
		Shader(const std::string& filePath);
		~Shader();

		void Bind();
		void Unbind();

		uint32_t GetProgramId() const { return m_ProgramID; }

		uint32_t GetUniformLocation(const char* name);

		void UploadUniformInt(const int value, int location);
		void UploadUniformInt(const int value, const char* name);

		void UploadUniformFloat(const float value, int location);
		void UploadUniformFloat(const float value, const char* name);
		void UploadUniformFloat2(const glm::vec2& value, int location);
		void UploadUniformFloat2(const glm::vec2& value, const char* name);
		void UploadUniformFloat3(const glm::vec3& value, int location);
		void UploadUniformFloat3(const glm::vec3& value, const char* name);
		void UploadUniformFloat4(const glm::vec4& value, int location);
		void UploadUniformFloat4(const glm::vec4& value, const char* name);

		void UploadUniformMat4(const glm::mat4& matrix, const char* name);

	private:
		std::string ReadFile(const std::string& filePath);
		std::unordered_map<GLenum, std::string> PreProcess(const std::string& source);
		void Compile(const std::unordered_map<GLenum, std::string>& sourceShaders);

		GLenum ShaderTypeFromToken(const std::string& token);

	private:
		uint32_t m_ProgramID = 0;
	};
}
