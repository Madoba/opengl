#pragma once

#include <cstdint>

namespace gl
{
	class IndexBuffer
	{
	public:
		IndexBuffer(const size_t& size);
		~IndexBuffer();

		void Bind() const;
		void Unbind() const;

		void SetData(const void* data, uint32_t size);

		uint32_t GetCount() const { return m_Count; }
	private:
		uint32_t m_VIB;
		uint32_t m_Count;
		uint32_t m_Offset = 0;
	};
}
