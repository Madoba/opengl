#include "Application.h"

#include <iostream>

#include "Events/Event.h"
#include "Events/ApplicationEvent.h"
#include "Events/KeyEvent.h"
#include "Events/MouseEvent.h"
#include "Events/Input.h"

namespace gl
{
	Application* Application::s_Instance = nullptr;

	void Application::Run()
	{
		s_Instance = this;
		bool fullscreen = false;
		m_Window = new Window(WindowInfo{ "Learning OpenGL", 1280, 720, fullscreen });
		m_Window->SetEventCallback([this](auto&&... args) -> decltype(auto)
		{
			return this->Application::OnEvent(std::forward<decltype(args)>(args)...);
		});
		m_ImGui = new ImGuiLayer;

		startup();

		while (m_Running)
		{
			render(glfwGetTime());
			OnImGuiRender();
			m_Window->OnUpdate();
		}
		shutdown();
	}

	void Application::startup()
	{
		m_ImGui->StartUp();

		m_ExamplesStates.reserve(27);
		m_ExamplesStates.push_back({ "FilledTriangle", false });
		m_ExamplesStates.push_back({ "Tesselated Triangle", false });
		m_ExamplesStates.push_back({ "Simple Texture", false });
		m_ExamplesStates.push_back({ "Spinning Cube", false });
		m_ExamplesStates.push_back({ "Texture KTX", false });
		m_ExamplesStates.push_back({ "Wrap Texture", false });
		m_ExamplesStates.push_back({ "Tunnel", false });
		m_ExamplesStates.push_back({ "Wrap Modes", false });
		m_ExamplesStates.push_back({ "Mirror Clamp Edge", false });
		m_ExamplesStates.push_back({ "Alien Rain", false });
		m_ExamplesStates.push_back({ "Fragment list - Not working as expected", false });
		m_ExamplesStates.push_back({ "Indexed cube", false });
		m_ExamplesStates.push_back({ "Grass", false });
		m_ExamplesStates.push_back({ "Instanced attributes", false });
		m_ExamplesStates.push_back({ "Multi draw indirect", false });
		m_ExamplesStates.push_back({ "Spring mass - Not working as expected", false });
		m_ExamplesStates.push_back({ "Clip Distance", false });
		m_ExamplesStates.push_back({ "Tessellation Modes", false });
		m_ExamplesStates.push_back({ "Displacement Map", false });
		m_ExamplesStates.push_back({ "Cubic Bezier", false });
		m_ExamplesStates.push_back({ "Geometry shader culling", false });
		m_ExamplesStates.push_back({ "Object exploding", false });
		m_ExamplesStates.push_back({ "Geometry shader tessellation", false });
		m_ExamplesStates.push_back({ "Normals viewer", false });
		m_ExamplesStates.push_back({ "Geometry shader quad", false });
		m_ExamplesStates.push_back({ "Multiple viewports", false });
		m_ExamplesStates.push_back({ "Multiple scissor", false });
	
		
		// Renderer Init
		// To display wireframe 
		//glPolygonMode(GL_FRONT_AND_BACK, GL_LINE);
		m_FilledTriangle = new FilledTriangle;
		m_TesselatedTriangle = new TessellatedTriangle;
		m_SimpleTexture = new SimpleTexture;
		m_SpinningCube = new SpinningCube;
		m_TextureKTX = new TextureKTX;
		m_WrapTexture = new WrapTexture;
		m_Tunnel = new Tunnel;
		m_WrapModes = new WrapModes;
		m_MirrorClampEdge = new MirrorClampEdge;
		m_AlienRain = new AlienRain;
		m_FragmentList = new FragmentList;
		m_IndexedCube = new IndexedCube;
		m_Grass = new Grass;
		m_InstancedAttributes = new InstancedAttributes;
		m_MultiDrawIndirect = new MultiDrawIndirect;
		m_SpringMass = new SpringMass;
		m_ClipDistance = new ClipDistance;
		m_TessModes = new TessModes;
		m_DisplacementMap = new DisplacementMap;
		m_CubicBezier = new CubicBezier;
		m_GeomShaderCulling = new GeomShaderCulling;
		m_ObjectExploder = new ObjectExploder;
		m_GeoShaderTessellate = new GeoShaderTessellate;
		m_NormalViewer = new NormalViewer;
		m_GeoShaderQuad = new GeoShaderQuad;
		m_MultiViewport = new MultiViewport;
		m_MultiScissor = new MultiScissor;
	}

	void Application::render(double currentTime)
	{
		float aspect = m_Window->GetWidth() / m_Window->GetHeight();
		glViewport(0, 0, m_Window->GetWidth(), m_Window->GetHeight());

		GLfloat backgroundColor[] = { 0.8f, 0.2f, 0.2f, 1.0f };
		glClearBufferfv(GL_COLOR, 0, backgroundColor);

		if (m_ExamplesStates[0].state)
			m_FilledTriangle->Draw(currentTime);
		if (m_ExamplesStates[1].state)
			m_TesselatedTriangle->Draw(currentTime);
		if (m_ExamplesStates[2].state)
			m_SimpleTexture->Draw();
		if (m_ExamplesStates[3].state)
			m_SpinningCube->Draw(currentTime, aspect);
		if (m_ExamplesStates[4].state)
			m_TextureKTX->Draw();
		if (m_ExamplesStates[5].state)
			m_WrapTexture->Draw(currentTime, aspect);
		if (m_ExamplesStates[6].state)
			m_Tunnel->Draw(currentTime, aspect);
		if (m_ExamplesStates[7].state)
			m_WrapModes->Draw();
		if (m_ExamplesStates[8].state)
			m_MirrorClampEdge->Draw(currentTime);
		if (m_ExamplesStates[9].state)
			m_AlienRain->Draw(currentTime);
		if (m_ExamplesStates[10].state)
			m_FragmentList->Draw(currentTime, aspect);
		if (m_ExamplesStates[11].state)
			m_IndexedCube->Draw(currentTime, aspect);
		if (m_ExamplesStates[12].state)
			m_Grass->Draw(currentTime, aspect);
		if (m_ExamplesStates[13].state)
			m_InstancedAttributes->Draw();
		if (m_ExamplesStates[14].state)
			m_MultiDrawIndirect->Draw(currentTime, aspect);
		if (m_ExamplesStates[15].state)
			m_SpringMass->Draw();
		if (m_ExamplesStates[16].state)
			m_ClipDistance->Draw(currentTime, aspect);
		if (m_ExamplesStates[17].state)
			m_TessModes->Draw();
		if (m_ExamplesStates[18].state)
			m_DisplacementMap->Draw(currentTime, aspect);
		if (m_ExamplesStates[19].state)
			m_CubicBezier->Draw(currentTime, aspect);
		if (m_ExamplesStates[20].state)
			m_GeomShaderCulling->Draw(currentTime, aspect);
		if (m_ExamplesStates[21].state)
			m_ObjectExploder->Draw(currentTime, aspect);
		if (m_ExamplesStates[22].state)
			m_GeoShaderTessellate->Draw(currentTime, aspect);
		if (m_ExamplesStates[23].state)
			m_NormalViewer->Draw(currentTime, aspect);
		if (m_ExamplesStates[24].state)
			m_GeoShaderQuad->Draw(currentTime, aspect);
		if (m_ExamplesStates[25].state)
			m_MultiViewport->Draw(currentTime, m_Window->GetWidth(), m_Window->GetHeight());
		if (m_ExamplesStates[26].state)
			m_MultiScissor->Draw(currentTime, m_Window->GetWidth(), m_Window->GetHeight());
		
		
	}

	void Application::OnImGuiRender()
	{
		m_ImGui->Begin();
		m_ImGui->OnRender(m_ExamplesStates);
		if (m_ExamplesStates[10].state)
			m_FragmentList->OnImGuiRender();
		if (m_ExamplesStates[14].state)
			m_MultiDrawIndirect->OnImGuiRender();
		if (m_ExamplesStates[15].state)
			m_SpringMass->OnImGuiRender();
		if (m_ExamplesStates[16].state)
			m_ClipDistance->OnImGuiRender();
		if (m_ExamplesStates[17].state)
			m_TessModes->OnImGuiRender();
		if (m_ExamplesStates[18].state)
			m_DisplacementMap->OnImGuiRender();
		if (m_ExamplesStates[19].state)
			m_CubicBezier->OnImGuiRender();
		if (m_ExamplesStates[24].state)
			m_GeoShaderQuad->OnImGuiRender();

		m_ImGui->End();
	}

	void Application::shutdown()
	{
		m_ImGui->OnShutDown();

		delete m_FilledTriangle;
		delete m_TesselatedTriangle;
		delete m_SimpleTexture;
		delete m_SpinningCube;
		delete m_TextureKTX;
		delete m_WrapTexture;
		delete m_Tunnel;
		delete m_WrapModes;
		delete m_MirrorClampEdge;
		delete m_AlienRain;
		delete m_FragmentList;
		delete m_IndexedCube;
		delete m_Grass;
		delete m_InstancedAttributes;
		delete m_MultiDrawIndirect;
		delete m_SpringMass;
		delete m_ClipDistance;
		delete m_TessModes;
		delete m_DisplacementMap;
		delete m_CubicBezier;
		delete m_GeomShaderCulling;
		delete m_ObjectExploder;
		delete m_GeoShaderTessellate;
		delete m_NormalViewer;
		delete m_GeoShaderQuad;
		delete m_MultiViewport;
		delete m_MultiScissor;

		delete m_Window;
	}

	void Application::OnEvent(Event& e)
	{
		EventDispatcher dispatcher(e);
		dispatcher.Dispatch<KeyPressedEvent>([this](auto&&) 
		{
			if(Input::IsKeyPressed(GL_KEY_ESCAPE))
				m_Running = false;
			return true;
		});

		dispatcher.Dispatch<WindowCloseEvent>([this](auto&&)
		{
			m_Running = false;
			return true;
		});

		m_ImGui->OnEvent(e);

		if (m_ExamplesStates[10].state)
			m_FragmentList->OnEvent(e);
	}

	void Application::setVsync(bool enable)
	{
		m_Info.flags.vsync = enable ? 1 : 0;
		glfwSwapInterval((int)m_Info.flags.vsync);
	}
}