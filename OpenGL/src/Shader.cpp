#include "Shader.h"
#include <iostream>
#include <fstream>

#include <glm/gtc/type_ptr.hpp>

namespace gl
{
	Shader::Shader(const std::string& filePath)
	{
		std::string source = ReadFile(filePath);
		std::unordered_map<GLenum, std::string> sourceShaders = PreProcess(source);
		Compile(sourceShaders);
	}

	Shader::~Shader()
	{
		glDeleteProgram(m_ProgramID);
	}

	void Shader::Bind()
	{
		glUseProgram(m_ProgramID);
	}

	void Shader::Unbind()
	{
		glUseProgram(0);
	}

	uint32_t Shader::GetUniformLocation(const char* name)
	{
		return glGetUniformLocation(m_ProgramID, name);
	}

	void Shader::UploadUniformInt(const int value, int location)
	{
		glUniform1i(location, value);
	}

	void Shader::UploadUniformInt(const int value, const char* name)
	{
		auto location = glGetUniformLocation(m_ProgramID, name);
		UploadUniformInt(value, location);
	}

	void Shader::UploadUniformFloat(const float value, int location)
	{
		glUniform1f(location, value);
	}

	void Shader::UploadUniformFloat(const float value, const char* name)
	{
		auto location = glGetUniformLocation(m_ProgramID, name);
		UploadUniformFloat(value, location);
	}

	void Shader::UploadUniformFloat2(const glm::vec2& value, int location)
	{
		glUniform2f(location, value.x, value.y);
	}

	void Shader::UploadUniformFloat2(const glm::vec2& value, const char* name)
	{
		auto location = glGetUniformLocation(m_ProgramID, name);
		UploadUniformFloat2(value, location);
	}

	void Shader::UploadUniformFloat3(const glm::vec3& value, int location)
	{
		glUniform3f(location, value.x, value.y, value.z);
	}

	void Shader::UploadUniformFloat3(const glm::vec3& value, const char* name)
	{
		auto location = glGetUniformLocation(m_ProgramID, name);
		UploadUniformFloat3(value, location);
	}

	void Shader::UploadUniformFloat4(const glm::vec4& value, int location)
	{
		glUniform4f(location, value.x, value.y, value.z, value.w);
	}

	void Shader::UploadUniformFloat4(const glm::vec4& value, const char* name)
	{
		auto location = glGetUniformLocation(m_ProgramID, name);
		UploadUniformFloat4(value, location);
	}

	void Shader::UploadUniformMat4(const glm::mat4& matrix, const char* name)
	{
		GLint location = glGetUniformLocation(m_ProgramID, name);
		glUniformMatrix4fv(location, 1, GL_FALSE, glm::value_ptr(matrix));
	}

	std::string Shader::ReadFile(const std::string& filePath)
	{
		std::string source;
		std::ifstream file(filePath, std::ios::in | std::ios::binary | std::ios::ate);
		if (file.is_open())
		{
			size_t size = file.tellg();
			source.resize(size);
			file.seekg(0, std::ios::beg);
			file.read(&source[0], size);
			file.close();
		}
		else
			std::cout << "Cound't open the shader file!\n";
		return source;
	}
	std::unordered_map<GLenum, std::string> Shader::PreProcess(const std::string& source)
	{
		std::unordered_map<GLenum, std::string> shaderSources;
		const char* typeToken = "#type";
		size_t typeTokenSize = strlen(typeToken);
		size_t pos = source.find(typeToken, 0);
		while (pos != std::string::npos)
		{
			size_t type_eol = source.find_first_of("\r\n", pos);
			size_t type_begin = pos + typeTokenSize + 1;

			std::string type = source.substr(type_begin, type_eol - type_begin);

			size_t shader_begin = source.find_first_not_of("\r\n", type_eol);
			pos = source.find(typeToken, shader_begin);

			std::string sourceShader = pos == std::string::npos ? source.substr(shader_begin) : source.substr(shader_begin, pos - shader_begin);
			shaderSources.insert({ ShaderTypeFromToken(type), sourceShader });
		}
		return shaderSources;
	}

	void Shader::Compile(const std::unordered_map<GLenum, std::string>& sourceShaders)
	{
		m_ProgramID = glCreateProgram();
		std::vector<GLuint> shadersIds;
		shadersIds.reserve(sourceShaders.size());
		for (auto& shader : sourceShaders)
		{
			GLenum type = shader.first;
			std::string sourceStr = shader.second;
			const GLchar* source = sourceStr.c_str();
			GLuint id = glCreateShader(type);
			glShaderSource(id, 1, &source, NULL);
			glCompileShader(id);

			GLint isCompiled = 0;
			glGetShaderiv(id, GL_COMPILE_STATUS, &isCompiled);
			if (isCompiled == GL_FALSE)
			{
				GLint maxLength = 0;
				glGetShaderiv(id, GL_INFO_LOG_LENGTH, &maxLength);

				std::vector<GLchar> infoLog(maxLength);
				glGetShaderInfoLog(id, maxLength, &maxLength, &infoLog[0]);

				glDeleteShader(id);

				std::cout << infoLog.data();
				std::cout << "Shader compilation failure!\n";
				break;
			}

			glAttachShader(m_ProgramID, id);
			shadersIds.push_back(id);
		}

		glLinkProgram(m_ProgramID);

		GLint isLinked = 0;
		glGetProgramiv(m_ProgramID, GL_LINK_STATUS, (int*)&isLinked);
		if (isLinked == GL_FALSE)
		{
			GLint maxLength = 0;
			glGetProgramiv(m_ProgramID, GL_INFO_LOG_LENGTH, &maxLength);

			// The maxLength includes the NULL character
			std::vector<GLchar> infoLog(maxLength);
			glGetProgramInfoLog(m_ProgramID, maxLength, &maxLength, &infoLog[0]);

			// We don't need the program anymore.
			glDeleteProgram(m_ProgramID);

			for (auto id : shadersIds)
				glDeleteShader(id);

			std::cout << infoLog.data();
			std::cout << "Shader link failure!\n";
			return;
		}

		for(auto& id : shadersIds)
			glDeleteShader(id);
	}

	GLenum Shader::ShaderTypeFromToken(const std::string& token)
	{
		if (token == "vertex")
			return GL_VERTEX_SHADER;
		else if (token == "tessellation")
			return GL_TESS_CONTROL_SHADER;
		else if (token == "evaluation")
			return GL_TESS_EVALUATION_SHADER;
		else if (token == "geometry")
			return GL_GEOMETRY_SHADER;
		else if (token == "fragment")
			return GL_FRAGMENT_SHADER;
		return GLenum();
	}

}