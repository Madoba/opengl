#type vertex
#version 450 core
in vec4 position;

out VS_OUT
{
	vec4 color;
} vs_out;

uniform mat4 mv_matrix;
uniform mat4 proj_matrix;

void main(void)
{
	gl_Position = proj_matrix * mv_matrix * position;
	vs_out.color = position * 2.0 + vec4(0.5, 0.5, 0.5, 0.0);
}

#type fragment
#version 450 core

out vec4 color;

in VS_OUT
{
	vec4 color;
} fs_in;

void main(void)
{
	color = fs_in.color;
}