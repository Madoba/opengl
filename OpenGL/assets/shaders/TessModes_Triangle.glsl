#type vertex
#version 450 core                                               
                                                                
void main(void)                                                 
{                                                               
    const vec4 vertices[4] = vec4[4] (vec4( 0.4, -0.4, 0.5, 1.0),  
                                      vec4(-0.4, -0.4, 0.5, 1.0),  
                                      vec4( 0.4,  0.4, 0.5, 1.0),  
                                      vec4(-0.4,  0.4, 0.5, 1.0)); 
                                                                
    gl_Position = vertices[gl_VertexID];                        
}

#type tessellation
#version 450 core                                                            
                                                                             
layout (vertices = 3) out;                                                   
                                                                             
void main(void)                                                              
{                                                                            
    if (gl_InvocationID == 0)                                                
    {                                                                        
        gl_TessLevelInner[0] = 5.0;                                          
        gl_TessLevelOuter[0] = 8.0;                                          
        gl_TessLevelOuter[1] = 8.0;                                          
        gl_TessLevelOuter[2] = 8.0;                                          
    }                                                                        
    gl_out[gl_InvocationID].gl_Position = gl_in[gl_InvocationID].gl_Position;
}

#type evaluation
#version 420 core                                          
                                                           
// layout (triangles, point_mode) in; // Point mode
layout (triangles) in; 
                                                           
void main(void)                                            
{                                                          
    gl_Position = (gl_TessCoord.x * gl_in[0].gl_Position) +
                  (gl_TessCoord.y * gl_in[1].gl_Position) +
                  (gl_TessCoord.z * gl_in[2].gl_Position); 
}                                                          

#type fragment
#version 450 core     
                      
out vec4 color;       
                      
void main(void)       
{                     
    color = vec4(1.0);
}                   
