#type vertex
#version 450 core
			
layout (location = 0) in vec4 u_offset;	
layout (location = 1) in vec4 u_color;

out vec4 vs_color;	

void main(void)
{
	const vec4 vertices[3] = vec4[3](	vec4(0.25, -0.25, 0.5, 1.0),
										vec4(-0.25, -0.25, 0.5, 1.0),
										vec4(0.25, 0.25, 0.5, 1.0));
				
	gl_Position = vertices[gl_VertexID] + u_offset;

	vs_color = u_color;
}

#type tessellation
#version 450 core
layout (vertices = 3) out;

in vec4 vs_color[];
patch out vec4 tcs_color;	

void main(void)
{
	if (gl_InvocationID == 0)
	{
		gl_TessLevelInner[0] = 5.0;
		gl_TessLevelOuter[0] = 5.0;
		gl_TessLevelOuter[1] = 5.0;
		gl_TessLevelOuter[2] = 5.0;
	}

	gl_out[gl_InvocationID].gl_Position = gl_in[gl_InvocationID].gl_Position;
	tcs_color = vs_color[gl_InvocationID];
}

#type evaluation
#version 450 core
layout (triangles, equal_spacing, cw) in;

patch in vec4 tcs_color;	
out vec4 tes_color;	

void main(void)
{
	gl_Position = (gl_TessCoord.x * gl_in[0].gl_Position +
					gl_TessCoord.y * gl_in[1].gl_Position +
					gl_TessCoord.z * gl_in[2].gl_Position);
	tes_color = tcs_color;
}

#type geometry
#version 450 core
layout (triangles) in;
layout (points, max_vertices = 3) out;

in vec4 tes_color[];
out vec4 geo_color;	

void main(void)
{
	int i;
	for (i = 0; i < gl_in.length(); i++)
	{
		gl_Position = gl_in[i].gl_Position;
		geo_color = tes_color[i];
		EmitVertex();
	}
}

#type fragment
#version 450 core
				
in vec4 geo_color;					

out vec4 color;
			 
void main(void)
{
	//color = vec4(0.0, 0.8, 1.0, 1.0); 
	color = geo_color;
}