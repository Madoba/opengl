#type vertex
#version 450 core                                               
                                                                
void main(void)                                                 
{                                                               
    const vec4 vertices[4] = vec4[4] (vec4( 0.4, -0.4, 0.5, 1.0),  
                                      vec4(-0.4, -0.4, 0.5, 1.0),  
                                      vec4( 0.4,  0.4, 0.5, 1.0),  
                                      vec4(-0.4,  0.4, 0.5, 1.0)); 
                                                                
    gl_Position = vertices[gl_VertexID];                        
}

#type tessellation
#version 450 core                                                            
                                                                             
layout (vertices = 4) out;                                                   
                                                                             
void main(void)                                                              
{                                                                            
    if (gl_InvocationID == 0)                                                
    {                                                                        
        gl_TessLevelOuter[0] = 5.0;                                          
        gl_TessLevelOuter[1] = 5.0;
    }                                                                        
    gl_out[gl_InvocationID].gl_Position = gl_in[gl_InvocationID].gl_Position;
}                                                                            

#type evaluation
#version 450 core                                                             
                                                                                      
layout (isolines) in;                                                                                                                                 
         
void main(void)                                                               
{    
    // Horizontal
    vec4 p1 = mix(gl_in[0].gl_Position, gl_in[1].gl_Position, gl_TessCoord.x);
    vec4 p2 = mix(gl_in[2].gl_Position, gl_in[3].gl_Position, gl_TessCoord.x);
    gl_Position = mix(p1, p2, gl_TessCoord.y);

    // Vertical
    //vec4 p1 = mix(gl_in[0].gl_Position, gl_in[1].gl_Position, gl_TessCoord.y);
    //vec4 p2 = mix(gl_in[2].gl_Position, gl_in[3].gl_Position, gl_TessCoord.y);
    //gl_Position = mix(p1, p2, gl_TessCoord.x);

    //Spiral
    //float r = (gl_TessCoord.y + gl_TessCoord.x / gl_TessLevelOuter[0]); 
    //float t = gl_TessCoord.x * 2.0 * 3.14159;                           
    //gl_Position = vec4(sin(t) * r, cos(t) * r, 0.5, 1.0);     
}                                                                                                                

#type fragment
#version 450 core     
                      
out vec4 color;       
                      
void main(void)       
{                     
    color = vec4(1.0);
}                     