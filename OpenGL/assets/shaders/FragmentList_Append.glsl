#type vertex
#version 450 core

layout (location = 0) in vec4 position;

uniform mat4 mvp;

out vec4 pos;
out vec4 color;


void main(void)
{
    vec4 p = mvp * position;

    gl_Position = p;
    color = vec4(1.0);
    pos = p / p.w;
}

#type fragment
#version 450 core

// Atomic counter for filled size
layout (binding = 0, offset = 0) uniform atomic_uint fill_counter;

// 2D image to store head pointers
layout (binding = 0, r32ui) coherent uniform uimage2D head_pointer;

// Shader storage buffer containing appended fragments
struct list_item
{
    vec4        color;
    float       depth;
    int         facing;
    uint        next;
};

layout (binding = 0, std430) buffer list_item_block
{
    list_item   item[];
};

// Input from vertex shader

in vec4 pos;
in vec4 color;

void main(void)
{
    ivec2 P = ivec2(gl_FragCoord.xy);

    uint index = atomicCounterIncrement(fill_counter);

    uint old_head = imageAtomicExchange(head_pointer, P, index);

    item[index].color = color;
    item[index].depth = gl_FragCoord.z;
    item[index].facing = gl_FrontFacing ? 1 : 0;
    item[index].next = old_head;
}