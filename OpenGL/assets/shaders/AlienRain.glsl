#type vertex
#version 450 core

layout (location = 0) in int alien_index;

out flat int alien;
out vec2 tc;

struct droplet_t
{
	float x_offset;
	float y_offset;
	float orientation;
	float unused;
};

layout (std140) uniform droplets
{
	droplet_t droplet[256];
};

void main(void)
{
	const vec2[4] position = vec2[4](	vec2(-0.5, -0.5),
										vec2( 0.5, -0.5),
										vec2(-0.5,  0.5),
										vec2( 0.5,  0.5));
	tc = position[gl_VertexID].xy + vec2(0.5);
	float co = cos(droplet[alien_index].orientation);
	float so = sin(droplet[alien_index].orientation);
	mat2 rot = mat2(vec2(co, so),
					vec2(-so, co));
	vec2 pos = 0.25 * rot * position[gl_VertexID];
	gl_Position = vec4(pos.x + droplet[alien_index].x_offset,
						pos.y + droplet[alien_index].y_offset,
						0.5, 1.0);
	alien = alien_index % 64;
}

#type fragment
#version 450 core

layout (location = 0) out vec4 color;

in flat int alien;
in vec2 tc;

layout (binding = 0) uniform sampler2DArray tex_aliens;
			 
void main(void)
{
	color = texture(tex_aliens, vec3(tc, float(alien)));
}