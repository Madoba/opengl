#type vertex
#version 450 core

in vec4 position;

void main(void)
{
	gl_Position = position;
}

#type fragment
#version 450 core
					
uniform sampler2D s;

out vec4 color;
			 
void main(void)
{
	color = texture(s, gl_FragCoord.xy / textureSize(s, 0));
}