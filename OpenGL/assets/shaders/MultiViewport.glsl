#type vertex
#version 420 core                                            
                                                             
in vec4 position;                                            
                                                             
out VS_OUT                                                   
{                                                            
    vec4 color;                                              
} vs_out;                                                    
                                                             
void main(void)                                              
{                                                            
    gl_Position = position;                                  
    vs_out.color = position * 2.0 + vec4(0.5, 0.5, 0.5, 0.0);
}                                                            

#type geometry
#version 420 core                                       
                                                        
layout (triangles, invocations = 4) in;                 
layout (triangle_strip, max_vertices = 3) out;          
                                                        
layout (std140, binding = 0) uniform transform_block    
{                                                       
    mat4 mvp_matrix[4];                                 
};                                                      
                                                        
in VS_OUT                                               
{                                                       
    vec4 color;                                         
} gs_in[];                                              
                                                        
out GS_OUT                                              
{                                                       
    vec4 color;                                         
} gs_out;                                               
                                                        
void main(void)                                         
{                                                       
    for (int i = 0; i < gl_in.length(); i++)            
    {                                                   
        gs_out.color = gs_in[i].color;                  
        gl_Position = mvp_matrix[gl_InvocationID] *     
                      gl_in[i].gl_Position;             
        gl_ViewportIndex = gl_InvocationID;             
        EmitVertex();                                   
    }                                                   
    EndPrimitive();                                     
}  

#type fragment
#version 420 core        
                         
out vec4 color;          
                         
in GS_OUT                
{                        
    vec4 color;          
} fs_in;                 
                         
void main(void)          
{                        
    color = fs_in.color; 
}                        