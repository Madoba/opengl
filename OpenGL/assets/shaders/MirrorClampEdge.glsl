#type vertex
#version 450 core

in vec4 v_Position;
out vec2 uv;

void main(void)
{
	gl_Position = v_Position;
	uv = v_Position.xy;
}

#type fragment
#version 450 core
				
layout (binding = 0) uniform sampler2D tex;	

layout (location = 0) out vec4 color;

in vec2 uv;
			 
void main(void)
{
	color = texture(tex, uv);
}