#type vertex
#version 450 core

layout (location = 0) in vec3 position;

void main(void)
{
    gl_Position = vec4(position * 0.03, 1.0);
}

#type fragment
#version 410 core

layout (location = 0) out vec4 color;

void main(void)
{
    color = vec4(1.0);
}