#type vertex
#version 450 core                                                              
                                                                               
// Incoming per vertex... position and normal                                  
layout (location = 0) in vec4 vVertex;                                         
layout (location = 1) in vec3 vNormal;                                         
                                                                               
out Vertex                                                                     
{                                                                              
    vec3 normal;                                                               
    vec4 color;                                                                
} vertex;                                                                      
                                                                               
uniform vec3 vLightPosition = vec3(-10.0, 40.0, 200.0);                        
uniform mat4 mvMatrix;                                                         
                                                                               
void main(void)                                                                
{                                                                              
    // Get surface normal in eye coordinates                                   
    vec3 vEyeNormal = mat3(mvMatrix) * normalize(vNormal);                     
                                                                               
    // Get vertex position in eye coordinates                                  
    vec4 vPosition4 = mvMatrix * vVertex;                                      
    vec3 vPosition3 = vPosition4.xyz / vPosition4.w;                           
                                                                               
    // Get vector to light source                                              
    vec3 vLightDir = normalize(vLightPosition - vPosition3);                   
                                                                               
    // Dot product gives us diffuse intensity                                  
    vertex.color = vec4(0.7, 0.6, 1.0, 1.0) * abs(dot(vEyeNormal, vLightDir)); 
                                                                               
    gl_Position = vVertex;                                                     
    vertex.normal = vNormal;                                                   
}           

#type geometry
#version 410 core                                                   
                                                                    
layout (triangles) in;                                              
layout (triangle_strip, max_vertices = 3) out;                      
                                                                    
in Vertex                                                           
{                                                                   
    vec3 normal;                                                    
    vec4 color;                                                     
} vertex[];                                                         
                                                                    
out vec4 color;                                                     
                                                                    
uniform vec3 vLightPosition;                                        
uniform mat4 mvpMatrix;                                             
uniform mat4 mvMatrix;                                              
                                                                    
uniform vec3 viewpoint;                                             
                                                                    
void main(void)                                                     
{                                                                   
    int n;                                                          
                                                                    
    vec3 ab = gl_in[1].gl_Position.xyz - gl_in[0].gl_Position.xyz;  
    vec3 ac = gl_in[2].gl_Position.xyz - gl_in[0].gl_Position.xyz;  
    vec3 normal = normalize(cross(ab, ac));                         
    vec3 transformed_normal = (mat3(mvMatrix) * normal);            
    vec4 worldspace = /* mvMatrix * */ gl_in[0].gl_Position;        
    vec3 vt = normalize(viewpoint - worldspace.xyz);                
                                                                    
    if (dot(normal, vt) > 0.0) {                                    
        for (n = 0; n < 3; n++) {                                   
            gl_Position = mvpMatrix * gl_in[n].gl_Position;         
            color = vertex[n].color;                                
            EmitVertex();                                           
        }                                                           
        EndPrimitive();                                             
    }                                                               
}      

#type fragment
#version 410 core         
                          
in vec4 color;            
                          
out vec4 output_color;    
                          
void main(void)           
{                         
    output_color = color; 
}                         