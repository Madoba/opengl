#type vertex
#version 450 core

uniform mat4 mv_matrix;
uniform mat4 proj_matrix;

in vec4 v_Position;

out vec2 v_TexCoord;

void main(void)
{
	vec4 pos_vs = mv_matrix * v_Position;
	gl_Position = proj_matrix * pos_vs;
	v_TexCoord = v_Position.xy * 3.0 + vec2(0.45 * 3);
	//v_TexCoord = gl_Position.xy * 3.0 + vec2(0.45 * 3);
}

#type fragment
#version 450 core

layout (binding = 0) uniform sampler2D tex_object;

in vec2 v_TexCoord;					

out vec4 color;
			 
void main(void)
{
	color = texture(tex_object, v_TexCoord);
}
