#type vertex
#version 410 core                                             
                                                              
layout (location = 0) in vec4 position;                       
layout (location = 1) in vec3 normal;                         
                                                              
out VS_OUT                                                    
{                                                             
    vec3 normal;                                              
    vec4 color;                                               
} vs_out;                                                     
                                                              
void main(void)                                               
{                                                             
    gl_Position = position;                                   
    vs_out.color = position * 2.0 + vec4(0.5, 0.5, 0.5, 0.0); 
    vs_out.normal = normalize(normal);                        
}                                                             

#type geometry
#version 410 core                                                      
                                                                       
layout (triangles) in;                                                 
layout (line_strip, max_vertices = 4) out;                             
                                                                       
uniform mat4 mv_matrix;                                                
uniform mat4 proj_matrix;                                              
                                                                       
in VS_OUT                                                              
{                                                                      
    vec3 normal;                                                       
    vec4 color;                                                        
} gs_in[];                                                             
                                                                       
out GS_OUT                                                             
{                                                                      
    vec3 normal;                                                       
    vec4 color;                                                        
} gs_out;                                                              
                                                                       
uniform float normal_length = 0.2;                                     
                                                                       
void main(void)                                                        
{                                                                      
    mat4 mvp = proj_matrix * mv_matrix;                                
    vec3 ab = gl_in[1].gl_Position.xyz - gl_in[0].gl_Position.xyz;     
    vec3 ac = gl_in[2].gl_Position.xyz - gl_in[0].gl_Position.xyz;     
    vec3 face_normal = normalize(cross(ab, ac));                      \
                                                                       
    vec4 tri_centroid = (gl_in[0].gl_Position +                        
                         gl_in[1].gl_Position +                        
                         gl_in[2].gl_Position) / 3.0;                  
                                                                       
    gl_Position = mvp * tri_centroid;                                  
    gs_out.normal = gs_in[0].normal;                                   
    gs_out.color = gs_in[0].color;                                     
    EmitVertex();                                                      
                                                                       
    gl_Position = mvp * (tri_centroid +                                
                         vec4(face_normal * normal_length, 0.0));      
    gs_out.normal = gs_in[0].normal;                                   
    gs_out.color = gs_in[0].color;                                     
    EmitVertex();                                                      
    EndPrimitive();                                                    
                                                                       
    gl_Position = mvp * gl_in[0].gl_Position;                          
    gs_out.normal = gs_in[0].normal;                                   
    gs_out.color = gs_in[0].color;                                     
    EmitVertex();                                                      
                                                                       
    gl_Position = mvp * (gl_in[0].gl_Position +                        
                         vec4(gs_in[0].normal * normal_length, 0.0));  
    gs_out.normal = gs_in[0].normal;                                   
    gs_out.color = gs_in[0].color;                                     
    EmitVertex();                                                      
    EndPrimitive();                                                    
}   

#type fragment
#version 410 core                                       
                                                        
out vec4 color;                                         
                                                        
in GS_OUT                                               
{                                                       
    vec3 normal;                                        
    vec4 color;                                         
} fs_in;                                                
                                                        
void main(void)                                         
{                                                       
    color = vec4(1.0) * abs(normalize(fs_in.normal).z); 
}                                                       