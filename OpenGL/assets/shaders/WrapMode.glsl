#type vertex
#version 450 core

uniform vec2 offset;

in vec4 v_Position;
out vec2 v_TexCoord;

void main(void)
{
	gl_Position = v_Position + vec4(offset, 0.0, 0.0);
	v_TexCoord = v_Position.xy * 3.0 + vec2(0.45 * 3);
}

#type fragment
#version 450 core

uniform sampler2D s;

in vec2 v_TexCoord;					

out vec4 color;
			 
void main(void)
{
	color = texture(s, v_TexCoord);
}