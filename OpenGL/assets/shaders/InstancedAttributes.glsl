#type vertex
#version 450 core

in vec4 position;
in vec4 instance_color;
in vec4 instance_position;

out vec4 vs_color;

void main(void)
{
	gl_Position = (position + instance_position) * vec4(0.25, 0.25, 1.0, 1.0);
	vs_color = instance_color;
}

#type fragment
#version 450 core
				
in vec4 vs_color;				

out vec4 color;
			 
void main(void)
{
	color = vs_color;
}