#type vertex
#version 450 core                                              
                                                               
layout (location = 0) in vec4 position;                        
layout (location = 1) in vec3 normal;                          
                                                               
out VS_OUT                                                     
{                                                              
    vec3 normal;                                               
    vec4 color;                                                
} vs_out;                                                      
                                                               
uniform mat4 mv_matrix;                                        
uniform mat4 proj_matrix;                                      
                                                               
void main(void)                                                
{                                                              
    gl_Position = proj_matrix * mv_matrix * position;          
    vs_out.color = position * 2.0 + vec4(0.5, 0.5, 0.5, 0.0);  
    vs_out.normal = normalize(mat3(mv_matrix) * normal);       
}                                                              

#type geometry
#version 450 core                                                  
                                                                   
layout (triangles) in;                                             
layout (triangle_strip, max_vertices = 3) out;                     
                                                                   
in VS_OUT                                                          
{                                                                  
    vec3 normal;                                                   
    vec4 color;                                                    
} gs_in[];                                                         
                                                                   
out GS_OUT                                                         
{                                                                  
    vec3 normal;                                                   
    vec4 color;                                                    
} gs_out;                                                          
                                                                   
uniform float explode_factor = 0.2;                                
                                                                   
void main(void)                                                    
{                                                                  
    vec3 ab = gl_in[1].gl_Position.xyz - gl_in[0].gl_Position.xyz; 
    vec3 ac = gl_in[2].gl_Position.xyz - gl_in[0].gl_Position.xyz; 
    vec3 face_normal = -normalize(cross(ab, ac));                  
    for (int i = 0; i < gl_in.length(); i++)                       
    {      
        gl_Position = gl_in[i].gl_Position + vec4(face_normal * explode_factor, 0.0);
        gs_out.normal = gs_in[i].normal;  
        gs_out.color = gs_in[i].color;    
        EmitVertex();                     
    }                                     
    EndPrimitive(); 
}

#type fragment
#version 410 core                                       
                                                        
out vec4 color;                                         
                                                        
in GS_OUT                                               
{                                                       
    vec3 normal;                                        
    vec4 color;                                         
} fs_in;                                                
                                                        
void main(void)                                         
{                                                       
    color = vec4(1.0) * abs(normalize(fs_in.normal).z); 
}                                                       