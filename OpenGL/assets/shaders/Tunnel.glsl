#type vertex
#version 450 core

in vec2 position;
out vec2 texCoord;

uniform mat4 viewMatrix;
uniform float offset;

void main(void)
{
	texCoord = (position + vec2(offset, 0.5)) * vec2(30.0, 1.0);  
	gl_Position = viewMatrix * vec4(position.xy, 0.0, 1.0);
}

#type fragment
#version 450 core
				
layout (location = 0) out vec4 color; 				

in vec2 texCoord;

layout (binding = 0) uniform sampler2D tex;
			 
void main(void)
{
	color = texture(tex, texCoord);    
}