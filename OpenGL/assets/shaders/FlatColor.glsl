#type vertex
#version 450 core
			
layout (location = 0) in vec3 u_Position;
layout (location = 1) in vec4 u_Color;

out vec4 vs_color;	

void main(void)
{
	const vec4 colors[] = vec4[3](vec4(1.0, 0.0, 0.0, 1.0),
							vec4(0.0, 1.0, 0.0, 1.0),
							vec4(0.0, 0.0, 1.0, 1.0));
	

	gl_Position = vec4(u_Position, 1.0);

	vs_color = colors[gl_VertexID];
}

#type fragment
#version 450 core
				
in vec4 vs_color;					

out vec4 color;
			 
void main(void)
{
	color = vs_color;
}
