#type vertex
#version 450 core                                               
                                                                
void main(void)                                                 
{                                                               
    const vec4 vertices[4] = vec4[4] (vec4( 0.4, -0.4, 0.5, 1.0),  
                                      vec4(-0.4, -0.4, 0.5, 1.0),  
                                      vec4( 0.4,  0.4, 0.5, 1.0),  
                                      vec4(-0.4,  0.4, 0.5, 1.0)); 
                                                                
    gl_Position = vertices[gl_VertexID];                        
}

#type tessellation
#version 450 core                                                            
                                                                             
layout (vertices = 4) out;                                                   
                                                                             
void main(void)                                                              
{                                                                            
    if (gl_InvocationID == 0)                                                
    {                                                                        
        gl_TessLevelInner[0] = 9.0;                                          
        gl_TessLevelInner[1] = 7.0;                                          
        gl_TessLevelOuter[0] = 3.0;                                          
        gl_TessLevelOuter[1] = 5.0;                                          
        gl_TessLevelOuter[2] = 3.0;                                          
        gl_TessLevelOuter[3] = 5.0;                                          
    }                                                                        
    gl_out[gl_InvocationID].gl_Position = gl_in[gl_InvocationID].gl_Position;
}                    

#type evaluation
#version 450 core                                                             
                                                                                      
layout (quads) in;                                                            
                                                                                      
void main(void)                                                               
{                                                                             
    vec4 p1 = mix(gl_in[0].gl_Position, gl_in[1].gl_Position, gl_TessCoord.x);
    vec4 p2 = mix(gl_in[2].gl_Position, gl_in[3].gl_Position, gl_TessCoord.x);
    gl_Position = mix(p1, p2, gl_TessCoord.y);                                
}                                                                                                                

#type fragment
#version 450 core     
                      
out vec4 color;       
                      
void main(void)       
{                     
    color = vec4(1.0);
}                     