workspace "OpenGL"
	architecture "x86_64"
	startproject "OpenGL"

	configurations
	{
		"Debug",
		"Release",
		"Dist"
	}
	
	flags
	{
		"MultiProcessorCompile"
	}

outputdir = "%{cfg.buildcfg}-%{cfg.system}-%{cfg.architecture}"

-- Include directories relative to root folder (solution directory)
IncludeDir = {}
IncludeDir["GLFW"] = "OpenGL/vendor/GLFW/include"
IncludeDir["Glad"] = "OpenGL/vendor/Glad/include"
IncludeDir["glm"] = "OpenGL/vendor/glm"
IncludeDir["ImGui"] = "OpenGL/vendor/imgui"

group "Dependencies"
	include "OpenGL/vendor/GLFW"
	include "OpenGL/vendor/Glad"
	include "OpenGL/vendor/imgui"

group ""

project "OpenGL"
	location "OpenGL"
	kind "ConsoleApp"
	language "C++"
	cppdialect "C++17"
	staticruntime "on"

	targetdir ("bin/" .. outputdir .. "/%{prj.name}")
	objdir ("bin-int/" .. outputdir .. "/%{prj.name}")

	files
	{
		"%{prj.name}/src/**.h",
		"%{prj.name}/src/**.cpp",
		"%{prj.name}/vendor/glm/glm/**.hpp",
		"%{prj.name}/vendor/glm/glm/**.inl"
	}

	defines
	{
		"_CRT_SECURE_NO_WARNINGS",
		"GLFW_INCLUDE_NONE"
	}

	includedirs
	{
		"%{prj.name}/src",
		"%{IncludeDir.GLFW}",
		"%{IncludeDir.Glad}",
		"%{IncludeDir.glm}",		
		"%{IncludeDir.ImGui}"
	}

	links 
	{ 
		"GLFW",
		"Glad",
		"ImGui",
		"opengl32.lib"
	}

	filter "system:windows"
		systemversion "latest"

	defines
	{
	}

	filter "configurations:Debug"
		defines "GL_DEBUG"
		runtime "Debug"
		symbols "on"

	filter "configurations:Release"
		defines "GL_RELEASE"
		runtime "Release"
		optimize "on"

	filter "configurations:Dist"
		defines "GL_DIST"
		runtime "Release"
		optimize "on"